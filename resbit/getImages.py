#!/usr/bin/env python

from HTMLParser import HTMLParser
from PIL import Image
from pprint import pprint
from operator import itemgetter
import cStringIO
import urlparse
import sys
import os
from resbit import url_helper


TMPL_BOOKMARK_FILE = '{token}_{num}{ext}'
TMPL_BOOKMARK_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static', 'pics', '{userID}')
TMPL_FEEDITEM_FILE = '{itemID}{ext}'
TMPL_FEEDITEM_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static', 'pics-feeds', '{feedID}')
IMAGE_TYPES = {
	'.jpg': 'JPEG',
	'.jpeg': 'JPEG',
	'.png': 'PNG'
}

# class Image(object):

# 	def __init__(self):

# 	def getAbsolutePath(self):

def process_candidates(candidates, base_url, **opts):
	CODE_GOOD = 0
	CODE_BAD_NAME = 8 # Using BAD_NAME_LIST
	CODE_BAD_EXT = 4 # Using ALLOWED_EST_LIST
	CODE_BAD_RATIO = 2 # Using BAD_RATIO
	CODE_BAD_DIMENSION = 16 # Using BAD_DIMENSION
	CODE_AD_BANNER = 32 # Using BAD_NAME_LIST
	CODE_BAD_RESOURCE = 64 # Can't get filetype or file doesn't exist or can't be processed, will overwrite other codes

	BAD_WORD_LIST = ('-sprite', 'sprite-', 'icon-', '-icon', 'icon_', '_icon', 'logo') 
	ALLOWED_EXT_LIST = ('.jpg', '.jpeg', '.png')
	IMAGE_ATTRS = ['src', 'content']
	BAD_DIMENSION = 250
	BAD_RATIO = 3

	# Defaults
	defaults = {
		'limit': None, 
		'code': 0
		}
	opts = dict(defaults.items() + opts.items())


	if len(candidates) == 0:
		return []
	
	images = list()
	for image in candidates:
		images.append({
			'src': dict([(key, image.get(attr)) for key, attr in enumerate(IMAGE_ATTRS) if image.get(attr)]).get(0), 
			'file': None,
			'alt': image.get('alt'), 
			'title': image.get('title'), 
			'ext': None,
			'type': None,
			'dimension': 0, 
			'width': 0,
			'height': 0,
			'code': CODE_GOOD, # no error codes
			})

	for image in images:

		if image['src']:
			image['src'] = url_helper.absolute_url(image['src'], base_url, no_query=False)
			image['file'] = os.path.split(url_helper.absolute_url(image['src'], base_url, no_query=True))[1]
		else:
			image['code'] = CODE_BAD_RESOURCE
			continue

		# Check name
		for word in BAD_WORD_LIST:
			if image['file'].find(word) >= 0:
				image['code'] += CODE_BAD_NAME
				break

		# Get extension, note some files may not have extension
		image['ext'] = os.path.splitext(image.get('file'))[1]
		if image['ext'] and image['ext'] not in ALLOWED_EXT_LIST:
			image['code'] += CODE_BAD_EXT

		# Get file type
		image['type'] = url_helper.get_mimetype(image.get('src'))
		if not image['type']:
			image['code'] = CODE_BAD_RESOURCE
			continue

		 # Check for image pixel area and dimension
		file = None
		img_resource = url_helper.load_resource(image['src'])
		if img_resource:
			file = cStringIO.StringIO(img_resource.read())
		else:
			# print >> sys.stderr, 'Problem opening image resource: %s' % image['src']
			image['code'] = CODE_BAD_RESOURCE
			continue

		if file:
			try:
				img = Image.open(file)
			except Exception, e:
				print >> sys.stderr, e
				continue
			x,y = img.size 
			image['width'] = x
			image['height'] = y
			image['dimension'] = x + y
			# If image is too small, disregard it
			if (x + y <= BAD_DIMENSION):
				image['code'] += CODE_BAD_DIMENSION

			ratio = 1
			if x > y:
				ratio = x/float(y)
			else:
				ratio = y/float(x)

			if ratio >= BAD_RATIO:
				image['code'] += CODE_BAD_RATIO

			file.close()

	# Sort images by dimension and error code
	images = sorted(images, key=itemgetter('dimension'), reverse=True)
	images = sorted(images, key=itemgetter('code'))
	
	if opts.get('getAll') == True:
		return images
	else:    
		return [image for image in images if image['code'] == opts['code']][:opts['limit']]

	return images


def find_image_candidates(soup, soup_chunk=None, **opts):
	"""
	1. First we will look for open graph formats
	<meta property="og:image" content="http://ia.media-imdb.com/images/rock.jpg" />

	2. Extract all images from the content

	"""
	images = None

	if not soup_chunk:
		images = soup.findAll('img')

	if soup_chunk:
		has_attrs = ['id', 'class']
		soup_all = soup_chunk.findAll()
		found_tag = None
		try:
			for tag in soup_all:
				for attr in has_attrs:
					if tag.has_attr(attr):
						found_tag = tag
						raise StopIteration
		except:
			pass

		soup_found_tag = None
		if found_tag:
			soup_found_tag = soup.find(name=found_tag.name, attrs=found_tag.attrs)

			if soup_found_tag and opts.get('direction') == 'up':
				images = soup_found_tag.findAll_previous('img')
			elif soup_found_tag and opts.get('direction') == 'down':
				images = soup_found_tag.findAll_next('img')
			elif soup_found_tag and opts.get('direction') == 'both':
				images = soup_found_tag.findAll_previous('img') + soup_found_tag.findAll_next('img')
	
	if not images:
		images = []

	return images


def find_ogimage_candidates(soup):
	"""
	Try to find images by looking at Open Graph tags
	"""
	images = soup.findAll(name='meta', attrs={'property': 'og:image'})
	
	return images




