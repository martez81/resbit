#!/usr/bin/env python
import os
import parseFeeds
import pydbwrapper
import pdb
import sys
import hashlib
import socket
import time
import traceback
from resbit import settings, tasks, models, celeryconfig
from celery import group, Celery

logger = tasks.logger
celery = Celery('tasks')
celery.config_from_object(celeryconfig)

@celery.task(ignore_result=True)
def scheduleTasks(limit=30, feedID = None, wait=False):
    """ Every instance runs this task to get the  
    records that will be processed next. This should run quite often
    to make sure nodes are always busy.
    """
    try:
        models.feeds.db.reuseConnection = False

        nodeName = socket.gethostname()
        if not nodeName or nodeName == '':
            print 'Cannot get node name.'
            return False
        
        feeds = models.feeds.Feeds()
        
        # default queue
        queue = 'feeds'

        # process only selected feed
        if feedID:
            queue = 'feeds_hipri'
            feedsToProcess = [{'feedID': int(feedID)}]
        else:
            # get all marked feeds to process
            feedsToProcess = feeds.getScheduledFeeds(nodeName, limit=limit)
            if not feedsToProcess:
                # secure a pool of records
                feeds.assignFeeds(nodeName, limit=limit)
                feedsToProcess = feeds.getScheduledFeeds(nodeName, limit=limit)

        timeStart = time.time()

        for feed in feedsToProcess:
            job = tasks.scheduleFeeds.apply_async((feed,queue), link=tasks.updateFeed.s())

            if wait:
                result = job.get()

                
    except:
        logger.error(traceback.format_exc())

@celery.task(ignore_result=True)
def queueFeeds(olderThan=3600, limit=None):
    """ Finds all feeds that need to be updated and puts
    them on the queue (in mysql table) signing them with server
    signature (hostname).
    """
    try:
        models.feeds.db.reuseConnection = False
        olderThan = int(olderThan)
        feeds = models.feeds.Feeds()
        feedsData = feeds.getFeeds(olderThan=olderThan, limit=limit, excludeScheduled=True)

        if feedsData:
            feeds.queueFeeds(feedsData)
            print('%s feeds queued at %s' % (len(feedsData), time.strftime('%Y-%m-%d %H:%M:%S')))

        return True
    except:
        logger.error(traceback.format_exc())

@celery.task(ignore_result=True)
def lookForTasks():
    """ Check if there is anything that can be processed (only signed record).
    We want to do this quite often but make sure server doesn't swell up.
    In order to make it safe server will has to have clear queue first in 
    order to put in more tasks.
    """
    try:
        nodeName = socket.gethostname()
        app = Celery('tasks')
        i = app.control.inspect()

        if (len(i.reserved()['celery@'+socket.gethostname()]) > 0):
            return
        else:
            scheduleTasks()
    except:
        logger.error(traceback.format_exc())


if __name__ == '__main__':
    import os
    activate_this = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../venv/bin/activate_this.py')
    execfile(activate_this, dict(__file__=activate_this))
    
    if len(sys.argv) == 2 and sys.argv[1].isdigit():
        scheduleTasks(0, sys.argv[1])
    elif len(sys.argv) == 2 and sys.argv[1] == 'lookForTasks':
        lookForTasks()
    elif len(sys.argv) == 2 and sys.argv[1] == 'queueFeeds':
        queueFeeds()
        
