#/usr/bin/env python
from resbit import settings
from resbit.common import getNodeName
from datetime import timedelta
import socket

"""
Useful settings
http://stackoverflow.com/questions/13275884/django-celery-periodic-tasks-run-but-rabbitmq-queues-arent-consumed
http://stackoverflow.com/questions/6362829/rabbitmq-on-ec2-consuming-tons-of-cpu

Celery best practices
https://denibertovic.com/posts/celery-best-practices/
"""

master_node = '127.0.0.1'
for node in settings.servers['task']:
	if node.get('role') == 'master':
		master_node = node

node_name = socket.gethostname()

beat_tasks = {
	'add-to-queue': {
        'task': 'resbit.scheduleTasks.queueFeeds',
        'schedule': timedelta(seconds=900)
    },
    'process-feeds': {
        'task': 'resbit.scheduleTasks.lookForTasks',
        'schedule': timedelta(seconds=150)
    }
}

if master_node['name'] == getNodeName():
	CELERYBEAT_SCHEDULE = {
		'add-to-queue': beat_tasks.get('add-to-queue'),
		'process-feeds': beat_tasks.get('process-feeds')	        
	}
else:
	CELERYBEAT_SCHEDULE = {
		'process-feeds': beat_tasks.get('process-feeds')	        
	}

# CELERY_IMPORTS = ('resbit.tasks')
BROKER_URL = 'amqp://marcin:martek123!@%s//' % master_node['address'] if master_node['name'] != node_name else '127.0.0.1' 
# See: http://docs.celeryproject.org/en/latest/configuration.html#broker-connection-max-retries
BROKER_CONNECTION_MAX_RETRIES = 3
# CELERY_RESULT_BACKEND = 'amqp://'
# Do not store tasks result
CELERY_IGNORE_RESULT = True
CELERY_TIMEZONE = 'America/New_York'
CELERY_ENABLE_UTC = False
CELERY_AUTOSCALE = 8,4
#CELERY_CREATE_MISSING_QUEUES = True
CELERY_ROUTES = {
    'resbit.tasks.scheduleItem': {'queue': 'feeds'},
    'resbit.tasks.scheduleFeeds': {'queue': 'feeds'},
    'resbit.tasks.updateFeed': {'queue': 'feeds'},
    'resbit.tasks.testTask': {'queue': 'feeds'},
    'resbit.scheduleTasks.queueFeeds': {'queue': 'beat'},
    'resbit.scheduleTasks.lookForTasks': {'queue': 'beat'}
}
CELERYD_CONCURRENCY = 8
CELERYD_TASK_TIME_LIMIT = 300
CELERY_SEND_TASK_ERROR_EMAILS = True
ADMINS = (('admin', 'marcin.kossakowski@gmail.com'),)
