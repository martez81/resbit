from resbit.models.modelBase import ModelBase

class FeedItem(ModelBase):
    
    def __init__(self, id=None):
        ModelBase.__init__(self, id)
        
        self.properties = {
            'itemID': None,
            'feedID': None,
            'itemHash': '',
            'itemTitle': '',
            'itemSummary': '',
            'itemContent': '',
            'itemUrl': '',
            'itemUrlID': '',
            'itemTags': '',
            'itemImage': '',
            'itemVideo': '',
            'itemImages': '',
            'itemAuthor': '',
            'itemPublished': '0000-00-00 00:00:00',
            'itemCreated': '0000-00-00 00:00:00'
        }

        # Initialize properties
        for property in self.properties:
            setattr(self, property, self.properties[property])
    
        
        self._tableName = 'FeedsItems'
        self._indexColumn = 'itemID'
        
        if id:
            self._id = id
            self.itemID = id
            self.read(id)

