from resbit import settings
from resbit import common
from resbit import db

class ModelBase(object):
    
    def __init__(self, id):
        self._id = id
        self.db = db
        self.createID = None
        
    def create(self):
        
        data = {}
        toInsert = []
        
        for property in self.properties:
            if (property == self._indexColumn): continue
            
            toInsert.append( '[{0}]'.format(property) ) # Build columns for the query
            data[property] = getattr(self, property) # Build data to insert based on object properties
        
        debug_log_format = 'some message'
        
        query = """
        INSERT INTO `{tableName}`
        SET
            {columns};
        """
        
        query = query.format(tableName=self._tableName, columns=','.join(toInsert))

        self.db.execute(query, data)
        self.createID = self.db.lastInsertId

        # load new data to object
        self.read(self.createID)
        
        return self.createID

    def read(self, id):
        
        queryTemplate = """
        SELECT
            *
        FROM `{tableName}`
        WHERE
            `{indexColumn}` = {id};
        """
        
        query = queryTemplate.format(
            tableName=self._tableName, 
            id=id,
            indexColumn=self._indexColumn
            )
        
        data = self.db.fetchFirst(query)
        
        if data:
            self._setProperties(data)
        else:
            return None
    
    def update(self):
            
        data = {}
        toUpdate = []

        for property in self.properties:
            toUpdate.append( '[%s]' % property ) # Build columns for the query
            data[property] = getattr(self, property) # Build data to insert based on object properties
        
        data[self._indexColumn] = self._id
        
        query = """
        UPDATE `{0}`
        SET
            {1}
        WHERE
            [{2}]
        LIMIT 1;
        """

        query = query.format(self._tableName, ','.join(toUpdate), self._indexColumn)
        
        self.db.execute(query, data)
    
    def delete(self):
        query = """
        DELETE FROM `{tableName}`
        WHERE
            `{indexColumn}` = {bookmarkID}
        LIMIT 1
        """
        query = query.format(
            tableName=self._tableName, 
            indexColumn=self._indexColumn,
            bookmarkID=self.bookmarkID
            )
        self.db.execute(query)
        self._unsetProperties()
    
    def _setProperties(self, data):
        
        if data:
            for col in data:
                if hasattr(self, col):
                    setattr(self, col, data[col])   
        else:
            return False

    def _unsetProperties(self):
        for prop in self.properties:
            if hasattr(self, prop):
                setattr(self, prop, None)