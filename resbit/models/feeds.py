from resbit import settings
from resbit import common
from resbit import db
from datetime import datetime
import time
import sphinxsearch

class Feeds():
    def __init__(self):

        self.db = db

    def getFeedsByUser(self, userID, **opts):
        limit = ''
        if opts.get('limit') and opts.get('limit') == 0:
            return dict()
        elif opts.get('limit'):
            limit = 'LIMIT %s' % opts['limit']

        feedID = ''
        if opts.get('feedID'):
            opts['feedID'] = [opts.get('feedID')] if type(opts.get('feedID')) != list else opts.get('feedID')
            feedID = 'AND f.`feedID` IN(%s)' % ','.join([str(x) for x in opts['feedID']])

        queryTemplate = """
        SELECT
            f.*
            ,DATE_FORMAT(f.`feedUpdatedOn`, '%b %e, %Y, %k:%i') as feedUpdatedOnFormatted
            ,uf.`userFeedShowMax`
            ,uf.`userFeedColor`
        FROM 
            `Feeds` AS f
        LEFT JOIN
            `UsersFeeds` AS uf ON uf.`feedID` = f.`feedID`
        WHERE
            uf.`userID` = {userID}
            AND f.`feedBanned` = 0
            {feedID}
        ORDER BY
            f.`feedName` ASC
        {limit}
        """


        query = queryTemplate.format(
            userID=userID,
            feedID=feedID,
            limit=limit
            )
        data = self.db.fetchAll(query)
        return data

    def searchFeeds(self, term, **opts):
        if not opts.get('limit'):
            opts['limit'] = 15

        try:
            s = sphinxsearch.SphinxClient()
            s.SetServer('127.0.0.1', 9312)
            s.SetMatchMode(sphinxsearch.SPH_MATCH_EXTENDED)
            s.SetLimits(0, opts.get('limit'))
            res = s.Query(term+'*')
        except Exception, e:
            return False

        results = []
        if res and res.get('matches'):
            data = [match['id'] for match in res.get('matches')]

            # Take the results from index and lookup items by ID
            if data:
                results = self.getFeeds(feedID=data)

        return results

    def getFeeds(self, **opts):
        default_order = 'f.`feedName` ASC'
        byFeedID = ''
        if opts.get('feedID'):
            if type(opts.get('feedID')) == list:
                byFeedID = 'AND f.`feedID` IN (%s)' % ','.join(str(x) for x in opts.get('feedID'))
                default_order = 'FIND_IN_SET(f.`feedID`, "%s" )' % ','.join(str(x) for x in opts.get('feedID'))
            else:
                byFeedID = 'AND f.`feedID` = %s' % int(opts['feedID'])

        byFeedUrl = ''
        if opts.get('feedUrl'):
            opts['limit'] = 1
            byFeedUrl = 'AND f.`feedUrl` LIKE "{feedUrl}%"'.format(feedUrl=opts['feedUrl'])

        olderThan = ''
        if opts.get('olderThan'):
            olderThan = 'AND UNIX_TIMESTAMP(f.`feedUpdatedOn`) <= UNIX_TIMESTAMP(NOW()) - %s' % int(opts.get('olderThan'))

        limit = ''
        if opts.get('limit') and opts.get('limit') == 0:
            return dict()
        elif opts.get('limit'):
            limit = 'LIMIT %s' % opts['limit']

        search = ''
        search_order = ''
        search_score = ''
        if False or opts.get('search'):
            search = 'AND (MATCH(f.`feedName`, f.`feedTitle`, f.`feedSubtitle`, f.`feedLink`, f.`feedTags`) AGAINST ("%s*"))' % opts.get('search') 
            search_order = '(MATCH(f.`feedName`, f.`feedTitle`, f.`feedSubtitle`, f.`feedLink`, f.`feedTags`) AGAINST ("%s*")) DESC, ' % opts.get('search') 

        excludeScheduled = ''
        excludeScheduledWhere = ''
        if opts.get('excludeScheduled'):
            excludeScheduled = 'LEFT JOIN `FeedsScheduled` AS fs ON fs.`feedID` = f.`feedID`'
            excludeScheduledWhere = 'AND fs.`feedID` IS NULL'

        queryTemplate = """
        SELECT
            f.*
            ,DATE_FORMAT(f.`feedUpdatedOn`, '%b %e, %Y, %k:%i') as feedUpdatedOnFormatted
            {search_score}
        FROM 
            `Feeds` AS f
            {excludeScheduled}
        WHERE
            f.`feedBanned` = 0
            {byFeedID}
            {byFeedUrl}
            {olderThan}
            {search}
            {excludeScheduledWhere}
        ORDER BY
            {search_order}
            {default_order}
        {limit}
        """

        query = queryTemplate.format(
            byFeedID=byFeedID,
            byFeedUrl=byFeedUrl,
            olderThan=olderThan,
            excludeScheduled=excludeScheduled,
            excludeScheduledWhere=excludeScheduledWhere,
            search=search,
            search_order=search_order,
            search_score=search_score,
            default_order=default_order,
            limit=limit
            )
        data = self.db.fetchAll(query)

        return data

    def getFeedItems(self, feedID, **opts):
        feedID = int(feedID)

        if opts.get('userID'):
            userID = common.getUserID()
        else:
            userID = 0
        # pdb.set_trace()
        limit = opts.get('limit', 5)

        byTags = ''
        if opts.get('tagList') and len(opts['tagList']) > 0:
            byTags = 'AND ' + ' OR '.join(['fi.`itemTitle` LIKE "%{0}%"'.format(tag) for tag in opts['tagList']])

        dates = ''
        if opts.get('dates'):
            dates = 'AND DATE(fi.`itemPublished`) IN ("%s")' % '","'.join(opts.get('dates'))

        between = ''
        if opts.get('between'):
            between = 'AND fi.`itemPublished` BETWEEN "%s" AND "%s"' % (opts['between'][0], opts['between'][1])

        pinned = ''
        if opts.get('pinned'):
            pinned = 'AND ufi.`userItemPinned` = 1'            


        queryTemplate = """
        SELECT
            fi.*
            ,f.`feedName`
            ,f.`feedLink`
            ,ufi.`userItemPinned`
        FROM 
            `Feeds` AS f
        JOIN
            `FeedsItems` AS fi ON fi.`feedID` = f.`feedID` 
        LEFT JOIN 
            `UsersFeedsItems` AS ufi ON ufi.`userID` = {userID} AND ufi.`itemID` = fi.`itemID`            
        WHERE
            fi.`feedID` = {feedID}
            {byTags}
            {pinned}
            {dates}
            {between}
        ORDER BY
            fi.`itemPublished` DESC
        {limit}
        """

        query = queryTemplate.format(
            feedID=feedID,
            limit='LIMIT ' + str(limit) if limit else '',
            byTags=byTags,
            userID=userID,
            pinned=pinned,
            dates=dates,
            between=between
            )

        data = self.db.fetchAll(query)
        return data        

    def getAnyRecentItems(self, **opts):
        limit = 15
        if opts.get('limit'):
            limit = opts['limit']

        query = """
        SELECT
            fi.*
            ,f.`feedName`
            ,f.`feedLink`
        FROM `FeedsItems` AS fi
            LEFT JOIN `Feeds` AS f ON f.`feedID` = fi.`feedID`
        ORDER BY
            fi.`itemPublished` DESC
        LIMIT {limit}
        """

        query = query.format(
            limit=limit
        )

        data = self.db.fetchAll(query)
        return data

    def getRecentItems(self, feedID, userID, **opts):
        upper_limit = time.mktime(time.localtime())
        lower_limit = upper_limit - 86400 # 24 hours
        return self.getFeedItems(feedID, limit = 5, between = (datetime.fromtimestamp(lower_limit), datetime.fromtimestamp(upper_limit)), userID = opts.get('userID'))

    def getOlderItems(self, feedID, **opts):
        upper_limit = time.mktime(time.localtime()) - 86400 # 24 hours
        lower_limit = upper_limit - 604800 # 7 days
        return self.getFeedItems(feedID, limit = 3, between = (datetime.fromtimestamp(lower_limit), datetime.fromtimestamp(upper_limit)), userID = opts.get('userID'))

    def itemExists(self, feedID, itemHash):

        query = """
        SELECT
            *
        FROM `FeedsItems` AS fi
        WHERE
            fi.`feedID` = {feedID}
            AND fi.`itemHash` = "{itemHash}"
        """

        query = query.format(feedID=feedID, itemHash=itemHash)

        data = self.db.fetchFirst(query)

        if data:
            return True
        else:
            return False

    def feedSubscribe(self, data):
        query = """
        REPLACE INTO `UsersFeeds` SET 
        [userID],[feedID];
        """
        self.db.execute(query, data)

    def itemTogglePinned(self, userID, itemID):

        userID = int(userID)
        itemID = int(itemID)

        if not userID or not itemID:
            return False

        queryTmpl = """
        SELECT
            *
        FROM `UsersFeedsItems` AS ufi
        WHERE
            ufi.`userID` = {userID}
            AND ufi.`itemID` = {itemID}
        """

        query = queryTmpl.format(userID=userID, itemID=itemID)
        data = self.db.fetchFirst(query)

        userItemPinned = '1'
        if data:
            if data['userItemPinned'] == 0:
                userItemPinned = '1'
            elif data['userItemPinned'] == 1:
                userItemPinned = '0'

            query = """
            UPDATE `UsersFeedsItems` SET [userItemPinned] WHERE [userID] AND [itemID]
            """
            self.db.execute(query, {'userItemPinned': userItemPinned, 'userID': userID, 'itemID': itemID})
        else:
            query = """
            INSERT INTO `UsersFeedsItems` SET [userID], [itemID], [userItemPinned]
            """
            self.db.execute(query, {'userItemPinned': userItemPinned, 'userID': userID, 'itemID': itemID})

        return userItemPinned

    def deleteItemsByUser(self, userID):
        userID = int(userID)
        
        query = """
        DELETE FROM `UsersFeeds` WHERE `userID` = {userID};
        """

        query = query.format(userID=userID)
        self.db.execute(query)

    def queueFeeds(self, feeds):
        query = """
        INSERT IGNORE INTO `FeedsScheduled`
        SET `feedID` = {feedID}, `scheduledOn` = NOW(), `scheduledBy` = "";
        """

        for feed in feeds:
            self.db.execute(query.format(feedID=feed['feedID']))

    def assignFeeds(self, nodeName, limit=None):
        query = """
        UPDATE `FeedsScheduled` 
        SET 
            `scheduledBy` = "{nodeName}" 
        WHERE 
            `scheduledBy` = "" 
        {limit};
        """

        self.db.execute(query.format(
            nodeName=nodeName, 
            limit='LIMIT ' + str(limit) if limit else ''
            )
        )

        return True

    def getScheduledFeeds(self, nodeName, limit, feedID=None):
        query = """
        SELECT 
            fs.*
        FROM 
            `FeedsScheduled` AS fs
        WHERE
            fs.`scheduledBy` = "{nodeName}"
            {whereFeedID}
        ORDER BY
            fs.`scheduledOn` ASC
        LIMIT {limit};
        """

        whereFeedID = ''
        if feedID:
            whereFeedID = 'AND fs.`feedID` = %d' % feedID

        data = self.db.fetchAll(query.format(
            nodeName=nodeName, 
            limit=limit,
            whereFeedID=whereFeedID
            )
        )
        return data

    def completeScheduledFeed(self, feedID):
        query = """
        DELETE FROM `FeedsScheduled`
        WHERE
            `feedID` = {feedID}
        """

        self.db.execute(query.format(feedID=feedID))

