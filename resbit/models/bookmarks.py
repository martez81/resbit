from resbit import common
from resbit import settings
from resbit import db

class Bookmarks():
    def __init__(self):

        self.db = db

    def getBookmarksByUserAndTags(self, userID, tagList=[], **kwargs):
        search = '1'
        if len(tagList) > 0:
            search = ' OR '.join(['t.`tagName` = "{0}"'.format(tag) for tag in tagList])
            if kwargs.get('expanded'):
                search += ' OR ' + ' OR '.join(['b.`bookmarkName` LIKE "%{0}%"'.format(tag) for tag in tagList])
                
        limit = ''
        if kwargs.get('limit'):
            limit = 'LIMIT ' + str(kwargs.get('limit'))

        queryTemplate = """
        SELECT
            b.*
            ,GROUP_CONCAT(`tagName`) AS tags
        FROM 
            `Bookmarks` AS b
            LEFT JOIN `TagsRelationships` AS tr ON tr.`bookmarkID` = b.`bookmarkID`
            LEFT JOIN `Tags` AS t ON t.`tagID` = tr.`tagID` 
            LEFT JOIN `Users` AS u ON u.`userID` = b.`userID`
        WHERE
            1
            AND b.`userID` = "{userID}"
            AND ({search})
        GROUP BY
            b.`bookmarkID`
        ORDER BY
            b.`bookmarkName` ASC
        {limit}
        """
        query = queryTemplate.format(
            search=search, 
            userID=userID, 
            limit=limit
            )
        data = self.db.fetchAll(query)

        return data
        
    def getBookmarksByUser(self, userID, **kwargs):
        user = 'AND u.`userID` = {0}'.format(userID)
        byReadLater = ''
        if kwargs.get('byReadLater'):
            byReadLater = 'AND b.`bookmarkReadLater` = 1'

        limit = ''
        if kwargs.get('limit'):
            limit = 'LIMIT ' + str(kwargs.get('limit'))
        
        queryTemplate = """
        SELECT
            b.*
        FROM 
            `Bookmarks` AS b
        LEFT JOIN 
            `Users` AS u ON u.`userID` = b.`userID`
        WHERE
            1
            {user}
            {byReadLater}
        GROUP BY
            b.`bookmarkID`
        ORDER BY
            b.`bookmarkName` ASC
        {limit}
        """
        query = queryTemplate.format(
            user=user, 
            byReadLater=byReadLater,
            limit=limit
            )
        data = self.db.fetchAll(query)

        return data
