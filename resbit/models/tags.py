from resbit import settings
from resbit import common
from resbit import db

class Tags():
    def __init__(self):
        self.db = db

    def getTagsByUserAndBookmark(self, userID=0, bookmarkID = 0):

        userID = int(userID)
        bookmarkID = int(bookmarkID)

        byUser = ''
        if userID > 0:
            byUser = 'AND t.`userID` = {0}'.format(userID)

        byBookmark = ''
        joinBookmark = ''
        if bookmarkID > 0:
            byBookmark = 'AND tr.`bookmarkID` = {0}'.format(bookmarkID)
            joinBookmark = 'JOIN `TagsRelationships` AS tr USING(`tagID`)'

        queryTemplate = """
        SELECT
            t.*
        FROM 
            `Tags` AS t
            {joinBookmark}
        WHERE
            `tagDeleted` = 0
            {byUser}
            {byBookmark}
        """

        query = queryTemplate.format(
            byUser=byUser, 
            byBookmark=byBookmark,
            joinBookmark=joinBookmark)
        data = self.db.fetchAll(query)
        return data

    def getTagsByUser(self, userID, **kwargs):
        byUser = 'AND t.`userID` = {0}'.format(userID)

        queryTemplate = """
        SELECT
            t.*
            ,COUNT(tr.`tagID`) AS tagsTotals
        FROM 
            `Tags` AS t
            LEFT JOIN `TagsRelationships` AS tr ON tr.`tagID` = t.`tagID` 
        WHERE
            1
            AND t.`tagDeleted` = 0
            {byUser}
        GROUP BY
            tr.`tagID`
        HAVING
            tagsTotals > 0
        ORDER BY
            t.`tagName` ASC
        """
        query = queryTemplate.format(byUser=byUser)
        data = self.db.fetchAll(query)
        return data