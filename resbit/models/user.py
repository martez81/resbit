from resbit.models.modelBase import ModelBase

class User(ModelBase):
    
    def __init__(self, id=None):
        ModelBase.__init__(self, id)
        
        self.properties= {'userID': None,
                            'userEmail': None,
                            'userPassword': None,
                            'userScreenName':None,
                            'userSalt': None,
                            'userCreatedOn': None,
                            'userDeleted': 0}
        # Initialize properties
        for property in self.properties:
            setattr(self, property, self.properties[property])
    
        
        self._tableName = 'Users'
        self._indexColumn = 'userID'
        
        if id:
            self._id = id
            self.userID = id
            self.read(id)
    
    def getUserByUsername(self, username):

        queryTemplate = """
        SELECT
            *
        FROM `{tableName}`
        WHERE
            `userEmail` = "{username}"
        LIMIT 1;
        """
        
        query = queryTemplate.format(
            tableName=self._tableName,
            username=username
        )

        data = self.db.fetchFirst(query)
        
        if data:
            self._setProperties(data)
        else:
            return None


    def getUserByEmail(self, email):
        
        queryTemplate = (
        'SELECT'
        '    *'
        'FROM `{tableName}`'
        'WHERE'
        '    `userEmail` = "{email}";'
        )
        query = queryTemplate.format(tableName=self._tableName, email=email)
        
        data = self.db.fetchFirst(query)
       
        if data:
            self._setProperties(data)
        else:
            return None


    def read(self, id):
        
        queryTemplate = (
        'SELECT'
        '    *'
        'FROM `{tableName}`'
        'WHERE'
        '    `{indexColumn}` = {id};'
        )
        
        query = queryTemplate.format(
            tableName=self._tableName,
            id=id,
            indexColumn=self._indexColumn
            )
        
        data = self.db.fetchFirst(query)
        
        if data:
            self._setProperties(data)
        else:
            return None