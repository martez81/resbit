from resbit.models.modelBase import ModelBase
from resbit import db
import time

class Signup(ModelBase):
    
    def __init__(self, id=None):
        ModelBase.__init__(self, id)
        
        self.properties = {
            'signupID': None,
            'email': 0,
            'createdOn': ''
        }

        # Initialize properties
        for property in self.properties:
            setattr(self, property, self.properties[property])
    
        
        self._tableName = 'Signups'
        self._indexColumn = 'signupID'
        
        if id:
            self._id = id
            self.feedID = id
            self.read(id)


def signupEmail(email):

    query = """
    SELECT 1 
    FROM  
        `Signups`
    WHERE
        `email` = "{email}"
    """
    query = query.format(email=email)
    record = db.fetchFirst(query)

    if record:
        return True
    else:
        query = """
        INSERT INTO 
            `Signups`
        SET
            `email` = "{email}"
            ,`createdOn` = "{createdOn}";
        """
        query = query.format(email=email, createdOn=time.strftime('%Y-%m-%d %H:%M:%S'))
        record = db.execute(query)

        return True
