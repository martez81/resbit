from resbit.models.modelBase import ModelBase

class Tag(ModelBase):
    
    def __init__(self, id=None):
        ModelBase.__init__(self, id)
        
        self.properties = {
                            'tagID': None,
                            'userID': None,
                            'tagName':None,
                            'tagColor':'#0088CC',
                            'tagDeleted': 0
        }

        # Initialize properties
        for property in self.properties:
            setattr(self, property, self.properties[property])
    
        
        self._tableName = 'Tags'
        self._indexColumn = 'TagID'
        
        if id:
            self._id = id
            self.tagID = id
            self.read(id)


    def read(self, id):
        queryTemplate = """
        SELECT
            *
        FROM `{tableName}`
        WHERE
            `{indexColumn}` = {id};
        """
        
        query = queryTemplate.format(
            tableName=self._tableName, 
            id=id,
            indexColumn=self._indexColumn
            )
        
        data = self.db.fetchFirst(query)
        
        if data:
            self._setProperties(data)
        else:
            return None


    def getTagByUserAndName(self, userID, tagName):
        queryTemplate = """
        SELECT
            *
        FROM `{tableName}`
        WHERE
            `userID` = {userID}
            AND `tagName` = "{tagName}"
            AND `tagDeleted` = 0;
        """
        
        query = queryTemplate.format(
            tableName=self._tableName, 
            tagName=tagName,
            userID=userID
            )
        
        data = self.db.fetchFirst(query)
        
        if data:
            self._setProperties(data)
        else:
            return None


    def createRelationshipForBookmark(self, tagID, bookmarkID):
        query = """
        REPLACE INTO `TagsRelationships`
        SET
            [tagID],[bookmarkID];
        """
        self.db.execute(query, {'tagID': tagID, 'bookmarkID': bookmarkID})
        return self.db.lastInsertId
