from resbit.models.modelBase import ModelBase

class Bookmark(ModelBase):
    
    def __init__(self, id=None):
        ModelBase.__init__(self, id)
        
        self.properties= {
            'bookmarkID': None,
            'userID': None,
            'bookmarkName': '',
            'bookmarkUrl': '',
            'bookmarkReadLater': 0,
            'bookmarkPrivate': 0,
            'bookmarkCreatedOn':'0000-00-00 00:00:00',
            'bookmarkDeleted': 0,
            'bookmarkNotes': '',
            'bookmarkImage': '',
            'bookmarkImageCDN': ''
            }

        # Initialize properties
        for property in self.properties:
            setattr(self, property, self.properties[property])
    
        
        self._tableName = 'Bookmarks'
        self._indexColumn = 'bookmarkID'
        
        if id:
            self._id = id
            self.bookmarkID = id
            self.read(id)


    def deleteTagsRelationships(self):
        query = """
        DELETE FROM `TagsRelationships`
        WHERE
            `bookmarkID` = {bookmarkID};
        """
        query = query.format(bookmarkID=self.bookmarkID)
        self.db.execute(query)
