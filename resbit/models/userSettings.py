from resbit.models.modelBase import ModelBase

class UserSettings(ModelBase):
    
    def __init__(self, id=None):
        ModelBase.__init__(self, id)
        
        self.properties = {
                            'userID': None,
                            'settingNotify': None,
                            'settingTimeZone':None,
                            'settingProfilePrivate':1
        }

        # Initialize properties
        for property in self.properties:
            setattr(self, property, self.properties[property])
    
        
        self._tableName = 'UserSettings'
        self._indexColumn = 'userID'
        
        if id:
            self._id = id
            self.tagID = id
            self.read(id)
