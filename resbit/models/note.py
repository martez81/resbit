from modelBase import ModelBase

class Note(ModelBase):
    
    def __init__(self, id=None):
        ModelBase.__init__(self, id)
        
        self.properties= {'noteID':None,
                          'bookmarkID':None,
                          'noteContent':None,
                          'noteCreatedOn':None,
                          'noteDeleted':0
                          }

        # Initialize properties
        for property in self.properties:
            setattr(self, property, self.properties[property])
    
        
        self._tableName = 'BookmarksNotes'
        self._indexColumn = 'bookmarkID' # becasue we read note based on bookmarID not noteID
        
        if id:
            self._id = id
            self.noteID = id
            self.read(id)
