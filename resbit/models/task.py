#!/usr/bin/env python
from resbit.models.modelBase import ModelBase

class Task(ModelBase):
    
    def __init__(self, id=None):
        ModelBase.__init__(self, id)
        
        self.properties= {
            'taskName': None,
            'taskUpdatedOn': None,
            'taskStaleInterval': 0,
            }

        # Initialize properties
        for property in self.properties:
            setattr(self, property, self.properties[property])
    
        
        self._tableName = 'Tasks'
        self._indexColumn = 'taskName'
        
        if id:
            self._id = id
            self.taskName = id
            self.read(id)