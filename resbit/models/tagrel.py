from resbit.models.modelBase import ModelBase

class TagRel(ModelBase):
    
    def __init__(self, id=None):
        ModelBase.__init__(self, id)
        
        self.properties= {'relID':None,
                          'tagID':None,
                          'bookmarkID':None
                          }

        # Initialize properties
        for property in self.properties:
            setattr(self, property, self.properties[property])
    
        
        self._tableName = 'TagsRelationships'
        self._indexColumn = 'relID'
        
        if id:
            self._id = id
            self.relID = id
            self.read(id)


    def read(self, id):
        
        queryTemplate = (
        'SELECT'
        '    *'
        'FROM `{tableName}`'
        'WHERE'
        '    `{indexColumn}` = {id};'
        )
        
        query = queryTemplate.format(
            tableName=self._tableName, 
            id=id,
            indexColumn=self._indexColumn
            )
        
        data = self.db.fetchFirst(query)
        
        if data:
            self._setProperties(data)
        else:
            return None