from resbit.models.modelBase import ModelBase
import time

class UserRecovery(ModelBase):
    
    def __init__(self, id=None):
        ModelBase.__init__(self, id)
        
        self.properties= {'recoveryID':None,
                          'userEmail':None,
                          'recoveryToken':None,
                          'recoveryCreatedOn':None
                          }

        # Initialize properties
        for property in self.properties:
            setattr(self, property, self.properties[property])
    
        
        self._tableName = 'UsersRecovery'
        self._indexColumn = 'recoveryID'
        
        if id:
            self._id = id
            self.noteID = id
            self.read(id)


    def getUserByResetToken(self, token):

        query = """
        SELECT
            u.`userID`
        FROM `UsersRecovery` AS ur
        LEFT JOIN `Users` AS u ON u.`userEmail` = ur.`userEmail`
        WHERE
            ur.`recoveryToken` = "{token}"
            AND UNIX_TIMESTAMP(ur.`recoveryCreatedOn`) + 86400 > {now}
        ORDER BY 
            ur.`recoveryCreatedOn` DESC;
        """
        query = query.format(token=token, now=time.time())
        return self.db.fetchFirst(query)
