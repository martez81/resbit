from resbit.models.modelBase import ModelBase

class Feed(ModelBase):
    
    def __init__(self, id=None):
        ModelBase.__init__(self, id)
        
        self.properties = {
            'feedID': None,
            'categoryID': 0,
            'feedName': '',
            'feedTitle': '',
            'feedSubtitle': '',
            'feedTags': '',
            'feedLink': '',
            'feedUrl': '',
            'feedImage': '',
            'feedBanned': 0,
            'feedUpdatedOn': '0000-00-00 00:00:00'
        }

        # Initialize properties
        for property in self.properties:
            setattr(self, property, self.properties[property])
    
        
        self._tableName = 'Feeds'
        self._indexColumn = 'feedID'
        
        if id:
            self._id = id
            self.feedID = id
            self.read(id)

