import logging.config
import os

LOG_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '%(asctime)s %(levelname)s [%(name)s: %(lineno)s] -- %(message)s', 
            'datefmt': '%m-%d-%Y %H:%M:%S'
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout'
        },
        'app': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(os.path.split(os.path.abspath(__file__))[0], '../logs/app.log'),
            'formatter': 'standard',
            'maxBytes': 2097152,
            'backupCount': 2
        },
        'tasks_general': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(os.path.split(os.path.abspath(__file__))[0], '../logs/tasks.log'),
            'formatter': 'standard',
            'maxBytes': 5242880,
            'backupCount': 5
        },
        'tasks_error': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(os.path.split(os.path.abspath(__file__))[0], '../logs/tasks_err.log'),
            'formatter': 'standard',
            'maxBytes': 2097152,
            'backupCount': 2
        }
    },
    'loggers': {
        'resbit.tasks': {
            'handlers': ['tasks_general', 'tasks_error', 'console'],
            'level': 'INFO',
            'propagate': True
        }
    }
}
