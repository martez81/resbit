#/usr/bin/env python
import os
import optparse
import time
import traceback
from celery import Celery, group
from resbit import parseFeeds, celeryconfig, models
from celery.utils.log import get_task_logger


logger = get_task_logger(__name__)

celery = Celery('tasks')
celery.config_from_object(celeryconfig)

@celery.task()
def testTask():
    """ Test tasks
    """
    logger.error('Test Task started')
    time.sleep(10)
    logger.error('Test Task finished')

@celery.task()
def scheduleItem(feedID, item):
    """ Process single item
    """
    try:
        result = parseFeeds.processItem(feedID, item)
        return result
    except:
        logger.error(traceback.format_exc(), extra={'feedID': feedID, 'item': item})

@celery.task()
def scheduleFeeds(feed, queue):
    """ Get the list of items/articles to process for specific feed
    """
    try:
        items = parseFeeds.itemsToProcess(feed['feedID'])

        tasklist = []
        for item in items:
            tasklist.append(scheduleItem.s(feed['feedID'], item))
            logger.info('Task scheduled for feedID %s.' % feed['feedID'])

        complete = False
        if len(tasklist) == 0:
            logger.info('Nothing to process for feed ID: {feedID}'.format(feedID=feed['feedID']))
            complete = True
        else:
            job = group(tasklist)
            job.apply_async(queue=queue)

        return feed['feedID']
    except:
        logger.error(traceback.format_exc(), extra={'feedData': feed})

@celery.task(ignore_result=True)
def updateFeed(feedID):
    """ This task is triggered when all items in the feed have been processed.
    """
    try:
        feeds = models.feeds.Feeds()
        f = models.feed.Feed(feedID)
        f.feedUpdatedOn = time.strftime('%Y-%m-%d %H:%M:%S')
        f.update()
        feeds.completeScheduledFeed(feedID)
        # print 'Finished feed ID %s in %s' % (feedID, time.time() - timeFeedStart)
        logger.info('Finished feed ID %s' % feedID)
        # print 'Finished all %d feeds in %s' % (len(feedsToProcess), time.time() - timeStart)
    except:
        logger.error(traceback.format_exc(), extra={'feedID': feedID})

if __name__ == '__main__':
    # activate_this = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../venv/bin/activate_this.py')
    # execfile(activate_this, dict(__file__=activate_this))
    # del activate_this

    p = optparse.OptionParser()

    p.add_option('--function', '-f')
    p.add_option('--feedid', default=None, help='Required only for "schedule"')
    options, args = p.parse_args()

    # if options.function:
    #   func = options.function

    #   if func == 'schedule':
    #       schedule(options.feedid)
        
