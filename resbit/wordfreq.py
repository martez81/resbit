from BeautifulSoup import BeautifulSoup, Comment
from operator import itemgetter
from HTMLParser import HTMLParser
import re
import sys
import urllib2
import pdb


coordinatingConjunctions = ['and', 'but', 'or', 'nor', 'for', 'yet', 'so']
subordinatingConjunctions = ['after','although','as','because','before','how','if','once','since','than','that','though','till','until','when','where','whether','while']
interjections = ['absolutely','achoo','ack','ahh','aha','ahem','ahoy','agreed','alas','alright','alrighty','alrighty-roo','alack','amen','anytime','argh','anyhoo','anyhow','attaboy','attagirl','awww','awful','bam','behold','bingo','blah','boo','bravo','cheers','crud','darn','dang','doh','drat','duh','eek','eh','gee','geepers','golly','goodnes','gosh','ha','hallelujah','hey','hi','hmm','huh','indeed','jeez','no','now','nah','oops','ouch','phew','please','rats','shoot','shucks','there','tut','uggh','waa','what','woah','woops','wow','yay','yes','yikes']
pronouns = ['i', 'me', 'you', 'your', 'she', 'her', 'he', 'his', 'him', 'it', 'they', 'them', 'we', 'us', 'our']
other = ['the', 'at', 'were', 'are', 'is', 'do', 'with', 'this', 'that']

def processUrl(url):
    wordFreq = {}
    wordExclude = coordinatingConjunctions + subordinatingConjunctions + interjections + pronouns + other
    
    if url[:8] == 'https://':
        pass
    elif url[:7] == 'http://':
        pass
    else:
        url = 'http://' + url
    
    try:
        html = urllib2.urlopen(url).read()
    except:
        return {'error': 'Could not process url'}

    try:    
        data = ''.join(html)
        soup = BeautifulSoup(data)
        pass
    except:
        return {'error': 'Could not parse markup'}            


    # Remove some common tags we don't wont to extract text from
    [s.extract() for s in soup('script')]
    [s.extract() for s in soup('noscript')]
    [s.extract() for s in soup('object')]
    [s.extract() for s in soup('style')]

    # Remove html comments
    # http://www.crummy.com/software/BeautifulSoup/bs3/documentation.html#Removing%20elements
    comments = soup.findAll(text=lambda text:isinstance(text, Comment))
    [comment.extract() for comment in comments] 
    
    # pdb.set_trace()
    try:
        text = soup.body.text
    except:
        return {'error': 'Could not parse markup'}  

    for el in text:
        el = re.sub('\s+', ' ', el)
        elPieces = el.split(' ')
        
        for piece in elPieces:
            piece = piece.strip()

            if len(piece) <= 2 or piece in wordExclude: continue

            # count word repetitions
            if piece in wordFreq:
                wordFreq[piece] += 1
            else:
                wordFreq[piece] = 1

    tags = {}
    for (key, value) in wordFreq.items():
        if value < 3: continue
        tags[key] = value

    tags = sorted(tags.items(), key=itemgetter(1), reverse=True)
    return {'tags': tags, 'title': soup.title.text}

if __name__ == '__main__':
    # url = raw_input('Url to process (no http://): ')
    print processUrl('www.dutchpoint.org/dutchpoint/')