#!/usr/bin/env python
from flask import Flask
# from flask import request, session, redirect, flash, render_template, get_flashed_messages, url_for, g
import os
import re
import sys
import datetime
import atexit
import logging

# add pyDbWrapper to module path - this is temporary
# sys.path.insert(0, '/home/marcin/pyDbWrapper')

from resbit import common, settings, logconfig

# setup logging
logging.config.dictConfig(logconfig.LOG_CONFIG)

# Create flask application object
app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = settings.secretKey
app.config.from_object(__name__)
db = common.setDb()


from resbit.views import index
from resbit.views import tags
from resbit.views import access
from resbit.views import feeds
from resbit.views import api

# # Finalize
# # Needed to always close database connection
# @app.teardown_request
# def finalize(exception=None):
#   db.close()

