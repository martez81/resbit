from flask import request, session, redirect, flash, render_template, get_flashed_messages, url_for, g
from resbit import common, app
from resbit.models.tags import Tags
from resbit.models.tag import Tag

"""This is the main page."""
@app.route('/bookmarks/tags', methods=['GET', 'POST'])
def tags():

    # If logged in redirect
    if not common.isLoggedIn():
        return redirect(url_for('index'))

    if request.method == 'POST':
        _save()     

    # Get flash message
    message = get_flashed_messages(with_categories=True)

    # Get all tags of this user
    tags = Tags()
    allTags = tags.getTagsByUser(common.getUserID())

    # Process the home page

    return render_template(
        'tags.html',
        message=message,
        session=session,
        tags=allTags
    )


def _save():
    tagIDs = request.form.getlist('tagID')
    tagNames = request.form.getlist('tagName')
    tagColors = request.form.getlist('tagColor')
    tagDelete = request.form.getlist('tagDelete')

    if len(tagIDs) == len(tagNames) == len(tagColors):
        # debug
        for i,tag in enumerate(tagIDs):
            t = Tag(tag)
            # Check access
            if t.userID != common.getUserID():
                flash(common.message['no_access'], 'error')
                return redirect(url_for('index'))

            t.tagName = tagNames[i]
            t.tagColor = tagColors[i]
            t.tagDeleted = 1 if tag in tagDelete else 0 
            t.update()

        flash(common.messages['record_updated'], 'success')
        return redirect(url_for('tags'))
    else:
        flash(common.messages['record_action_error'], 'error')
        return redirect(url_for('tags'));