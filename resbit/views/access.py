from flask import request, session, redirect, flash, render_template, get_flashed_messages, url_for, g
from resbit import common, settings, app
from resbit.models.user import User
from resbit.models.userRecovery import UserRecovery
import resbit.models.signups as signups
import bcrypt
import re

@app.route('/login', methods=['GET', 'POST'])
def login():
    """Login."""

    if common.isLoggedIn():
        return redirect(url_for('index'))

    if request.form.get('returnUrl'):
        redirectUrl = request.form.get('returnUrl')
    else:
        redirectUrl = url_for('feeds_latest')

    ### Process credentials
    if len(request.form) > 0:
        userEmail = request.form.get('userEmail')
        userPassword = request.form.get('userPassword')

        if userEmail and userPassword:
            u = User()
            u.getUserByUsername(userEmail)

            if u.userID and (u.userPassword == bcrypt.hashpw(userPassword, u.userSalt)):
                common.setUser(u)

                # if userEmail == settings.demoUser:
                #     session['demoMode'] = True
                #     common.setDb('demo')

                flash('You were logged in successfully', category='success')
                return redirect(redirectUrl)
                
            else:
                flash(
                    '<b>Email or password entered did not match.</b> '
                    'Please try again. If you have forgotten your password '
                    '<a href="/resetpassword">click here</a> to recover it.', 
                    category='error')
                return redirect(redirectUrl)
        else:
            flash('Please enter email address and password.', category='error')
            return redirect(redirectUrl)

@app.route('/signup', methods=['GET', 'POST'])
def signup():

    if request.form.get('signupEmail'):
        email = request.form.get('signupEmail')

        EMAIL_REGEX = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
        if EMAIL_REGEX.match(email):
            signups.signupEmail(email)
            flash('You\'ve been signed up. Thank you!', category='success')
        else:
            flash('Seems like you did not provide valid email address. Please try again.', category='error')

    else:
        flash('Seems like you did not provide valid email address. Please try again.', category='error')
    
    return redirect(url_for('index'))


@app.route('/logout')
def logout():
    """Logout."""
    common.setDb()
    common.clearSession()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    message = get_flashed_messages(with_categories=True)

    userEmail = request.form.get('userEmail')
    userPassword = request.form.get('userPassword')

    if len(request.form) > 0:
        if userEmail and userPassword:

            if not common.validateEmail(userEmail):
                flash('Email address you entered seems to be invalid. Please try again.', category='error')
                return redirect(url_for('register'))

            # Check if email is not taken already
            u = User()
            u.getUserByEmail(userEmail)
            if u.userID:
                flash('This email address already exists. If you\'ve forgotten your password we can <a href="/resetpassword">reset it for you</a>.', category='error')
                return redirect(url_for('register'))

            # Create new user
            userSalt = bcrypt.gensalt(11)
            userPassword = bcrypt.hashpw(userPassword, userSalt)

            u = User()
            u.userEmail = userEmail
            u.userSalt = userSalt
            u.userPassword = userPassword
            u.userScreenName = userEmail.split('@')[0]
            u.userCreatedOn = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            u.create()

            if u.userID:
                flash('Your account has been created successfully!. You can now log in.', category='success')
                return redirect(url_for('index'))
            else:
                flash('There was a problem with creating this account. Please try again.', category='error')
        else:
            flash('One or more required fields are missing input. Please try again.', category='error')

    return render_template(
        'register.html',
        message=message,
        session=session,
        userEmail=userEmail
    )

@app.route('/resetpassword', methods=['GET', 'POST'])
@app.route('/resetpassword/<token>', methods=['GET', 'POST'])
def reset(token=False):
    message = get_flashed_messages(with_categories=True)
    mode = 'resetRequest'

    # If token was passed we will validate it
    # and based on that determine if user can
    # go ahead and reset the password
    if token:
        ur = UserRecovery()

        # Validate the token
        if ur.getUserByResetToken(token):
            mode = 'resetContinue'
        else:
            flash('Seems like password reset link expired. You can request new password reset.', 'error')
            return redirect(url_for('index'))


    if request.method == 'POST' and request.form.get('token'):
        password        = request.form.get('userPassword').strip()
        passwordRepeat  = request.form.get('userPasswordRepeat').strip()
        token           = request.form.get('token')

        if (password and passwordRepeat) and (password == passwordRepeat):
            ur      = UserRecovery()
            user    = ur.getUserByResetToken(request.form.get('token'))

            if user:
                secret = request.form.get('userPassword')
                u = User(user['userID'])
                u.userSalt = bcrypt.gensalt(11)
                u.userPassword = bcrypt.hashpw(password, u.userSalt)
                u.update()

                flash('Your password has been reset successfully. Now you can log in.', 'success')
            else:
                flash('Invalid token supplied.', 'error')   

            return redirect(url_for('index'))
        else:
            flash('Passwords in both fields have to match. Please try again', 'error')
            return redirect('/resetpassword/'+token)


    if request.method == 'POST' and mode == 'resetRequest':
        userEmail = request.form.get('userEmail').strip()

        if userEmail:
            # Check if that user exists
            u = User()
            u.getUserByEmail(userEmail)

            if not u.userID:
                flash('Your email address is not on file. Please create an account.')
                return redirect(url_for('index'))

            # User exists then, great
            # proceed with password reset
            ur                      = UserRecovery()
            ur.userEmail            = request.form['userEmail']
            ur.recoveryToken        = bcrypt.gensalt(11)
            ur.recoveryCreatedOn    = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            ur.create()

            common.sendMessage('Reset your password', "\r\n".join(common.emails['passwordRecovery']).format(token=ur.recoveryToken), ur.userEmail)

            flash('Your request was submitted successfully. You should receive email message with further instructions shortly.')
            return redirect(url_for('index'))
        else:
            flash('Please provide email address', 'error')
            return redirect(url_for('reset'))


    return render_template(
        'resetpassword.html',
        token=token,
        mode=mode,
        message=message,
        session=session
        )
