from flask import request, session, redirect, flash, render_template, get_flashed_messages, url_for, g
from resbit.models.bookmarks import Bookmarks
from resbit.models.bookmark import Bookmark
from resbit.models.user import User
from resbit.models.tags import Tags
from resbit.models.tag import Tag
from resbit.models.feeds import Feeds
from resbit.models.feed import Feed
from resbit.models.feedItem import FeedItem
from resbit import common, settings, app, getImages
from shutil import copyfile
from operator import itemgetter
from datetime import datetime, timedelta, date
import time
import urllib
import os
import re

app.jinja_env.globals.update(common=common, settings=settings)
app.jinja_env.filters['urlencode'] = urllib.quote

@app.route('/', methods=['GET', 'POST'])
def index():
    """This is the main page."""

    # Get flash message
    message = get_flashed_messages(with_categories=True)

    # Process the home page
    return render_template(
        'index.html',
        message=message,
        session=session,
        returnUrl=request.args.get('return') if request.args.get('return') else ''
    )

@app.route('/search')
def search():
    return main(mode='search')

@app.route('/article/<feedItemID>', methods=['GET'])
def feeditem(feedItemID):
    if feedItemID.isdigit():
        feedItemID = int(feedItemID)
    else:
        return index.error_page(common.messages['bad_request'])

    # Get flash message
    message = get_flashed_messages(with_categories=True)

    feedItem = FeedItem(feedItemID)

    feed = None
    if feedItem:
        feed = Feed(feedItem.feedID)

    return render_template(
        'feed_item.html',
        message     = message,
        session     = session,
        feedItem    = feedItem,
        feed        = feed
    )

@app.route('/articles/readlater')
def feeds_readlater():
    if not common.isLoggedIn():
        return common.requestAuthentication()
    return main(mode='feeds_readlater')

@app.route('/articles/latest')
def feeds_latest():
    if not common.isLoggedIn():
        return common.requestAuthentication()
    return main(mode='feeds_latest')

@app.route('/articles/<int:feedID>', methods=['GET'])
def feed_items(feedID):
    if feedID:
        feedID = int(feedID)
        return main(mode='feeds_id', feedID=feedID)
    else:
        return error_page(common.messages['bad_request'])

@app.route('/bookmarks')
def bookmarks_all():
    if not common.isLoggedIn():
        return common.requestAuthentication()
    return main(mode='bookmarks_all')

@app.route('/bookmarks/pinned')
def bookmarks_pinned():
    if not common.isLoggedIn():
        return common.requestAuthentication()
    return main(mode='bookmarks_pinned')

@app.route('/bookmarks/delete/<bookmarkID>')
def delete(bookmarkID):
    """Delete bookmark and clean up"""

    if not common.isLoggedIn():
        return common.requestAuthentication()

    if bookmarkID.isdigit():
        bookmarkID = int(bookmarkID)
    else:
        return error_page(common.messages['bad_request'])

    b = Bookmark(bookmarkID)
    if b.bookmarkID:
        # Check ownership
        if b.userID != common.getUserID():
            flash(common.messages['no_access'], 'error')
            return redirect(url_for('index'))

        b.deleteTagsRelationships()
        b.delete()
        flash(common.messages['record_deleted'], category='error')
        return redirect(url_for('index'))
    else:
        flash(common.messages['record_action_error'], category='error')
        return redirect(url_for('index'))

@app.route('/bookmarks/add', methods=['GET', 'POST'])
def add():
    """Add new bookmark."""

    if not common.isLoggedIn():
        return common.requestAuthentication()

    # Get flash message
    message = get_flashed_messages(with_categories=True)
    url = ''

    if request.args.get('url'):
        url = request.args.get('url')

    # Process POST data
    if request.method == 'POST':
        save()
        return redirect(url_for('bookmarks_pinned'))
    # or continue

    # Build bdata
    bdata = {}
    bdata['bookmarkUrl'] = url
    bdata['bookmarkName'] = ''

    return render_template(
        'addedit.html',
        message=message,
        bookmark=bdata,
        bookmarkToken=os.urandom(8).encode('hex'), # random token 
        mode='add',
        session=session
    )

@app.route('/bookmarks/edit/<bookmarkID>', methods=['GET', 'POST'])
def edit(bookmarkID):
    if not common.isLoggedIn():
        return common.requestAuthentication()
    # Check if user can access this data
    if bookmarkID.isdigit():
        bookmarkID = int(bookmarkID)

        b = Bookmark(bookmarkID)
        if b.userID != common.getUserID():
            flash(common.messages['no_access'], 'error')
            return redirect(url_for('index'))
    else:
        return error_page(common.messages['bad_request'])

    # Get flash message
    message = get_flashed_messages(with_categories=True)

    # Process POST data
    if request.method == 'POST':
        save()
        return redirect('/bookmarks/edit/%s' % bookmarkID)
        # else continue

    t = Tags()
    tdata = t.getTagsByUserAndBookmark(session['userID'], b.bookmarkID)

    return render_template(
        'addedit.html',
        message=message,
        bookmark=b,
        tags=tdata,
        mode='edit',
        bookmarkToken=os.urandom(8).encode('hex'), # random token 
        session=session
    )

def main(mode, feedID=None):
    message = get_flashed_messages(with_categories=True)
    u = User(common.getUserID())

    q = request.args.get('q')
    tag = request.args.get('tag')

    ### Get the list of tags for left panel
    allTags = Tags()
    allTagsData = allTags.getTagsByUser(common.getUserID())

    ### Get all feeds for left panel
    f = Feeds()
    userFeeds = f.getFeedsByUser(common.getUserID())

    items = ''
    if mode == 'feeds_latest':
        items = main_feeds(
            search_term     = q,
            mode            = mode,
            limit           = 5
            )
    elif mode == 'feeds_readlater':
        items = main_feeds(
            search_term     = q,
            mode            = mode,
            limit           = 10
            )
    elif mode == 'feeds_id' and feedID:
        items = main_feeds(
            search_term     = q,
            mode            = mode,
            feedID          = feedID,
            limit           = 50
            )
    elif mode == 'bookmarks_pinned':
        items = main_bookmarks(
            search_term     = q,
            search_tag      = tag,
            mode            = mode,
            )
    elif mode == 'bookmarks_all':
        items = main_bookmarks(
            search_term     = q,
            search_tag      = tag,
            mode            = mode,
            limit           = 50
            )
    elif mode == 'search':
        pass

    return render_template(
        'main.html',
        searchMode      = True if q else False,
        searchString    = q,
        tagMode         = True if tag else False,
        tagString       = tag,
        allTags         = allTagsData,
        userFeeds       = userFeeds,
        user            = u,
        items           = items.decode('utf-8'),
        message         = message,
        request         = request,
        session         = session
    )

def main_bookmarks(search_term=None, search_tag=None, mode=None, limit=None):
    title = 'All Bookmarks'
    if search_term:
        # Get bookmarks based on user and tags but also look in titles
        tagList = re.findall(r'\w+', search_term)
        b = Bookmarks()
        bdata = b.getBookmarksByUserAndTags(common.getUserID(), tagList, expanded=True, limit=50)
    elif search_tag:
        # Get bookmarks based on user and tag
        tagList = [search_tag]
        b = Bookmarks()
        bdata = b.getBookmarksByUserAndTags(common.getUserID(), tagList, limit=50)
    else:
        bdata = []

    ### Get all bookmark in read later category

    ### Not search mode
    if not bdata:
        b = Bookmarks()
        if mode == 'bookmarks_all':
            title = 'All Bookmarks'
            bdata = b.getBookmarksByUser(common.getUserID(), byReadLater=False, limit=50)
        elif mode == 'bookmarks_pinned':
            title = 'Bookmarks To Read Later'
            bdata = b.getBookmarksByUser(common.getUserID(), byReadLater=True)

    ### Get tags for each bookmark
    if bdata:
        for key, value in enumerate(bdata):
            t = Tags()
            tdata = t.getTagsByUserAndBookmark(common.getUserID(), bdata[key]['bookmarkID'])
            if tdata:
                bdata[key]['tags'] = tdata
            else:
                bdata[key]['tags'] = {}

    return render_template(
        'widget_bookmarks.html',
        title=title,
        bookmarks=bdata,
        total=len(bdata)
    ).encode('utf-8')

def main_feeds(search_term=None, limit=None, mode=None, feedID=None):
    tagList = []
    if search_term:
        tagList = re.findall(r'\w+', search_term)

    f = Feeds()
    fi = tuple()
    fi_old = tuple()

    if mode == 'feeds_readlater':
        title       = 'Feeds to read later'
        userFeeds   = f.getFeedsByUser(common.getUserID())
        for feed in userFeeds:
            fi = fi + f.getFeedItems(feed['feedID'], limit=None, tagList=tagList, pinned=True, userID=common.getUserID()) 
    
    elif mode == 'feeds_latest':
        title       = 'Most recent articles'
        userFeeds   = f.getFeedsByUser(common.getUserID(), feedID=feedID)
        for feed in userFeeds:
            fi = fi + f.getRecentItems(feed['feedID'], tagList=tagList, userID=common.getUserID())
            fi_old = fi_old + f.getOlderItems(feed['feedID'], tagList=tagList, userID=common.getUserID())

        fi = sorted(fi, key=itemgetter('itemPublished'), reverse=True)
        fi_old = sorted(fi_old, key=itemgetter('itemPublished'), reverse=True)

    elif mode == 'feeds_id':
        userFeeds = f.getFeeds(feedID=feedID)
        if not userFeeds:
            title = ''
            fi = ()
        else:
            title = 'Latest articles for %s' % userFeeds[0]['feedTitle']
            fi = f.getFeedItems(userFeeds[0]['feedID'], limit=75, tagList=tagList, userID=common.getUserID())

        
    return render_template(
        'widget_feeds.html',
        mode=mode,
        title=title,
        userFeeds=userFeeds,
        feeds=fi,
        feedsOld=fi_old,
        today=time.strftime('%Y-%m-%d', time.localtime()),
        today_human = time.strftime('%A, %B %d, %Y')

    ).encode('utf-8')

def save():
    """Save new bookmark."""

    if not common.isLoggedIn():
        return common.requestAuthentication()

    if request.form['bookmarkName']:
        # update bookmark
        redirect_to = None
        if request.form.get('bookmarkID'):
            redirect_to = 'edit'
            b = Bookmark(request.form.get('bookmarkID'))
            b.bookmarkName = request.form.get('bookmarkName')
            b.bookmarkUrl = common.fixUrl(request.form.get('bookmarkUrl')) if request.form.get('bookmarkUrl') else ''
            b.bookmarkNotes = request.form.get('bookmarkNotes')
            b.bookmarkReadLater = request.form.getlist('bookmarkReadLater')[-1] # get last element 

            if request.form.get('bookmarkImage'): # if image was selected
                b.bookmarkImage = request.form.get('bookmarkImage')
            b.update()
            flash('Bookmark updated successfully', category='success')
        # create new bookmark
        else :
            redirect_to = 'add'
            b = Bookmark()
            b.bookmarkName = request.form.get('bookmarkName')
            b.bookmarkUrl = request.form.get('bookmarkUrl')
            b.bookmarkNotes = request.form.get('bookmarkNotes')
            b.bookmarkReadLater = request.form.getlist('bookmarkReadLater')[-1]
            b.userID = session['userID']
            b.bookmarkCreatedOn = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            b.create()
            flash('New bookmark has been added successfully', category='success')

        # if image was selected
        if request.form.get('bookmarkImage') and b.bookmarkID: 
            b = Bookmark(b.bookmarkID)
            b.bookmarkImage = request.form.get('bookmarkImage')
            b.update()
            
        # flush tag - bookmark relationship 
        b.deleteTagsRelationships()

        # save the tags for new or existing record
        if 'tags' in request.form:
            tags = request.form.getlist('tags')

            # TODO: need a method in tag, add tag by name and bookmark
            for tag in tags:
                tag = tag.strip() # make sure there is no extra space around the keyword
                t = Tag()
                t.getTagByUserAndName(b.userID, tag)

                if not t.tagID:
                    t = Tag()
                    t.tagName = tag
                    t.userID = b.userID
                    t.create()                    
                    t.createRelationshipForBookmark(t.createID, b.bookmarkID)
                else:
                    t.createRelationshipForBookmark(t.tagID, b.bookmarkID)
        return True
    else:
        flash('One or more required fields are missing input. Please try again', category='error')
        return False

def error_page(message, error_code=404):
    return render_template(
        'error_page.html',
        error_message=message,
        error_code=error_code
    ), error_code