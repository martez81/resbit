from bookmarks import *

"""This is the main page."""
@app.route('/settings', methods=['GET', 'POST'])
def settings():

    # If logged in redirect
    if not common.isLoggedIn():
        return redirect(url_for('index'))

    # Get flash message
    message = get_flashed_messages()

    # Process the home page

    return render_template(
        'settings.html',
        message=message,
        session=session
    )

