from flask import request, session, redirect, flash, render_template, get_flashed_messages, url_for, g
from resbit import common, settings, url_helper, getImages, wordfreq, scheduleTasks, parseFeeds, app
from resbit.models.tags import Tags
from resbit.models.feed import Feed
from resbit.models.feeds import Feeds
from resbit.models.feedItem import FeedItem
from resbit.models.bookmark import Bookmark
import json
import time
import urllib2
import datetime
import BeautifulSoup as bs4

def authenticate():
    resp = {
        'responseMessage': 'success',
        'content': None  
    }

    if (not common.isLoggedIn() and int(common.getUserID()) != int(userID)) :
        resp['responseMessage'] = 'Authentication error'
        return json.dumps(resp), 501

@app.route('/api/users/<userID>/tags')
def userTags(userID):
    authenticate()

    resp = {
        'responseMessage': 'success',
        'content': None  
    }

    t = Tags()
    tdata = t.getTagsByUserAndBookmark(userID=userID)

    resp['content'] = tdata
    return json.dumps(resp), 202

@app.route('/api/bookmarks/<bookmarkID>/tags')
def bookmarkTags(bookmarkID):
    authenticate()

    resp = {
        'responseMessage': 'success',
        'content': None  
    }

    t = Tags()
    tdata = t.getTagsByUserAndBookmark(bookmarkID=bookmarkID)

    resp['content'] = tdata
    return json.dumps(resp), 202

@app.route('/api/processUrl')
def processUrl():
    authenticate()

    resp = {
        'responseMessage': 'success',
        'content': None  
    }

    if request.args.get('url') and common.getUserID():
        urlData = wordfreq.processUrl(request.args.get('url'))
        t = Tags()
        tdata = t.getTagsByUserAndBookmark(userID=common.getUserID())

        if 'tags' in urlData:
            data = []
            for tag in urlData['tags']:
                # Find matching tags
                if tdata and tag[0] in [t['tagName'] for t in tdata]:
                    data.append({'tagName': tag[0]})

            resp['content'] = {'tags': data, 'title': urlData.get('title')}
            return json.dumps(resp), 202
        else:
            resp['content'] = {'tags': None, 'title': urlData.get('title')}
            return json.dumps(resp), 202
    else:
        return json.dumps(resp), 202

@app.route('/api/getImages/<token>')
def getUrlImages(token):
    authenticate()

    resp = {
        'responseMessage': 'success',
        'content': None  
    }

    url = request.args.get('url', '').strip()

    if not url:
        return json.dumps(resp), 202

    base_url = url_helper.get_host_for_url(url)

    url_resource = url_helper.load_resource(url)
    html = url_resource.read()

    soup_full = bs4.BeautifulSoup(html)

    candidates_full = getImages.find_image_candidates(soup_full)
    candidates_ogimage = getImages.find_ogimage_candidates(soup_full)

    images = getImages.process_candidates(candidates_full + candidates_ogimage, base_url=base_url, limit=3)

    if images:
        resp['content'] = images
        return json.dumps(resp), 202
    else:
        return json.dumps(resp), 202

@app.route('/api/togglePinnedBookmark/<bookmarkID>')
def togglePinnedBookmark(bookmarkID):
    authenticate()

    resp = {
        'responseMessage': 'success',
        'content': None  
    }

    bookmarkID = int(bookmarkID)
    bookmark = Bookmark(bookmarkID)

    if not bookmark.bookmarkID:
        resp['responseMessage'] = 'error'
        return json.dumps(resp)

    if bookmark.bookmarkReadLater == 1:
        bookmark.bookmarkReadLater = 0
        resp['content'] = '0'
    else:
        bookmark.bookmarkReadLater = 1
        resp['content'] = '1'

    bookmark.update()
    return json.dumps(resp)

@app.route('/api/togglePinnedFeedItem/<itemID>')
def togglePinnedFeedItem(itemID):
    authenticate()
    resp = {
        'responseMessage': 'success',
        'content': None  
    }

    itemID = int(itemID)
    userID = common.getUserID()
    feeds = Feeds()

    data = feeds.itemTogglePinned(userID=userID, itemID=itemID)

    if not data:
        resp['responseMessage'] = 'error'
        return json.dumps(resp)

    if data == '1':
        resp['content'] = '1'
    elif data == '0':
        resp['content'] = '0'

    return json.dumps(resp)

@app.route('/api/feed/<feedID>')
def getFeedData(feedID):
    authenticate()
    resp = {
        'responseMessage': 'success',
        'content': None  
    }

    feedID = int(feedID)
    feeds = Feeds()
    feedData = feeds.getFeeds(feedID=feedID)

    if feedData:
        if not feedData[0]['feedUpdatedOn']:
            feedData[0]['feedUpdatedOn'] = datetime.datetime.fromtimestamp(0)

        if not feedData[0]['feedUpdatedOnFormatted']:
            feedData[0]['feedUpdatedOnFormatted'] = ''

        feedData[0]['feedUpdatedOnUnixtime'] = int(time.mktime(feedData[0]['feedUpdatedOn'].timetuple()))
        feedData[0]['feedUpdatedOn'] = feedData[0]['feedUpdatedOn'].strftime('%Y-%m-%d %H:%M:%S')

        resp['content'] = feedData[0]
    else:
        resp['responseMessage'] = 'error'

    return json.dumps(resp)

@app.route('/api/feed_refresh/<feedID>')
def feedRefresh(feedID):
    authenticate()
    feedID = int(feedID)

    resp = {
        'responseMessage': 'success',
        'content': None  
    }

    result = scheduleTasks.scheduleTasks(300, feedID)

    if result:
        return getFeedData(feedID)
    else:
        return json.dumps(resp)

@app.route('/api/find_feeds', methods=['GET'])
def findFeeds():
    # authenticate()
    resp = {
        'responseMessage': 'success',
        'content': None  
    }

    term = request.args.get('q')

    if not term:
        return json.dumps(resp)

    term = term.strip()
    term_has_spaces = True if term.find(' ') >= 0 else False
    term_has_dot = True if term.find('.') >= 0 else False

    result = []
    
    # Step 1
    # Term may have spaces "web development"
    # Try to search database
    # if term_has_spaces:
    feeds = Feeds()
    result = feeds.searchFeeds(term, limit=15)

    # Step 2
    # If contains http always process as url
    # term has no spaces but has a dot "development.com"?
    if not result and (term.find('https://') >= 0 or term.find('http://') >= 0 or term_has_dot):
        term = url_helper.fix_url_schema(term)
        parse_result = parseFeeds.feedFinder(term)
        if parse_result:
            result.append(parse_result)

    # Step 3
    # If still no results and
    # term has no spaces try treating as url by appending .com
    if (not result and not term_has_spaces):
        term = url_helper.fix_url_schema(term+'.com')
        parse_result = parseFeeds.feedFinder(term)

        if parse_result:
            result.append(parse_result)

    # Convert or values to string
    if result:
        result = [dict([(k[0], unicode(k[1])) for k in val.items()]) for val in result]

    resp['content'] = result
    return json.dumps(resp)

@app.route('/api/getFrontPageItems', methods=['GET'])
def getFrontPageItems():
    feeds = Feeds()
    items = feeds.getAnyRecentItems()

    return render_template(
        'partial_recent_feeditems.html',
        items=items

    ).encode('utf-8')

