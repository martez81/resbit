from flask import request, session, redirect, flash, render_template, get_flashed_messages, url_for, g
from resbit import common, settings, app, parseFeeds, scheduleTasks
from resbit.models.feeds import Feeds
from resbit.models.feed import Feed
from resbit.models.feedItem import FeedItem
from resbit.views import index
import pdb
import urllib2
import time

app.jinja_env.filters['format_content'] = common.format_content

"""This is the main page."""
@app.route('/articles/sources', methods=['GET', 'POST'])
def feeds():
    # If logged in redirect
    if not common.isLoggedIn():
        return redirect(url_for('index'))

    # If POST, save feeds settings
    if request.method == 'POST' and request.form.getlist('feedID'):
        _save()
    
    # Get flash message
    message = get_flashed_messages(with_categories=True)

    feeds = Feeds()
    allFeeds = feeds.getFeeds()
    userFeeds = feeds.getFeedsByUser(common.getUserID())

    mergedFeeds = []
    for af in allFeeds:
        for uf in userFeeds:
            if af['feedID'] == uf['feedID']:
                break
        else:
            mergedFeeds.append(af)

    return render_template(
        'feeds.html',
        message     = message,
        session     = session,
        url         = request.args['url'] if request.args.get('url') else '',
        feeds       = mergedFeeds,
        userFeeds   = userFeeds
    )

@app.route('/source-subscribe', methods=['GET'])
def feedSubscribe():
    if not common.isLoggedIn():
        return common.requestAuthentication()

    feeds = Feeds()
    userID = common.getUserID()
    feedUrl = request.args.get('url')
    feedID = request.args.get('id')

    if feedUrl:
        feedID = _saveNewFeed(feedUrl)

    if feedID:
        feeds.feedSubscribe({
            'userID': userID,
            'feedID': feedID
            })

        flash(common.messages['record_updated'], category='success')
    
    return redirect('/articles/sources#subscribed=%s' % feedID)

def _saveNewFeed(url):
    info = parseFeeds.feedInfo(url)
    
    # Check if have required info returned
    if not info.get('feedTitle') or not info.get('feedUrl'):
        flash(common.messages['record_action_error'], category='error')
        return redirect(url_for('feeds'))

    # Check if this feed already exists
    feeds = Feeds()
    feedsData = feeds.getFeeds(feedUrl=info.get('feedUrl'))

    if feedsData:
        flash(common.messages['feeds_feed_exist_error'], category='error')
        return redirect(url_for('feeds'))

    feed = Feed()
    feed.feedName = info.get('feedTitle')
    feed.feedTitle = info.get('feedTitle')
    feed.feedSubtitle = info.get('feedSubtitle')
    feed.feedUrl = info.get('feedUrl')
    feed.feedImage = info.get('feedImage')
    feed.feedLink = info.get('feedLink')
    feed.feedCreatedOn = time.strftime('%Y-%m-%d %H:%M:%S')
    feed.create()

    scheduleTasks.scheduleTasks(feedID=feed.createID)

    return feed.createID

def _search():
    pass

def _save():
    # Validate
    userID = common.getUserID()
    feedIDs = request.form.getlist('feedID')
    userFeedShowMaxs = request.form.getlist('userFeedShowMax')
    userFeedColors = request.form.getlist('userFeedColor')
    feedSubscriptions = request.form.getlist('feedSubscribe')

    feeds = Feeds()
    feeds.deleteItemsByUser(common.getUserID())

    if feedIDs:
        for i, feedID in enumerate(feedIDs):
            if feedID not in feedSubscriptions:
                continue

            feeds.feedSubscribe({
                'userID': userID,
                'feedID': feedIDs[i]
                })
        flash(common.messages['record_updated'], category='success')
        return redirect(url_for('feeds'))
    else:
        flash(common.messages['record_action_error'], category='error')
        return redirect(url_for('feeds'))        


