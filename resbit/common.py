#!/usr/bin/env python
from flask import session, flash, redirect, url_for, request
from pydbwrapper import pydbwrapper as dbw
from resbit import settings
import unicodedata
import hashlib
import re
import smtplib
import string
import time
import socket

# CONFIG FUNCTIONS
def getMasterForType(node_type):
    for node in settings.servers[node_type]:
        if node.get('role') == 'master':
            return node

def getNodeName():
    return socket.gethostname()

def setDb(config=None):
    if not config:
        config = settings.dbConfig

    master = getMasterForType('database')
    if master['name'] != getNodeName():
        config['host'] = master['address']

    db = dbw.PyDbWrapper.singleton(config)
    db.debugMode = False

    return db


# SITE FUNCTIONS
def isLoggedIn():
    """Use to check if user is logged in."""
    if session.has_key('userID'):
        return True
    else:
        return False

def setUser(u):
    session.clear()
    session['userID'] = u.userID
    session['userName'] = u.userEmail

def getUserID():
    if session.has_key('userID'):
        return session['userID']
    else:
        return False

def getSessionData():
    return session

def clearSession():
    session.clear()

def urlSlug(string):
    slug = string
    slug = slug.encode('ascii', 'ignore').lower()
    slug = re.sub(r'[^a-z0-9]+', '-', slug).strip('-')
    slug = re.sub(r'[-]+', '-', slug)
    return slug

def requestAuthentication():
    flash('Please log in first')
    return redirect('/?return=' + request.url)

def validateEmail(email):
    if len(email) > 7:
        if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email) != None:
            return True
            
    return False

def sendMessage(subject, message, toEmail, fromEmail=settings.adminEmail):
    
    body = string.join((
        'From: %s' % fromEmail,
        'To: %s' % toEmail,
        'Subject: %s' % subject ,
        '',
        message
        ), "\r\n")

    server = smtplib.SMTP('localhost')
    server.sendmail(fromEmail, [toEmail], body)
    server.quit()

def fixUrl(url):
    if url.find('http://') == 0 or url.find('https://') == 0:
        return url
    else:
        return 'http://' + url

def format_content(content):
    return "<br />".join(content.split("\n"))

# MESSAGES
messages = {
    'bad_request': 'There was a problem processing this request.',
    'no_access': 'You have no permission to access that.',
    'record_updated': 'Changes saved successfully.',
    'record_deleted': 'Record has been deleted.',
    'record_created': 'New record has been added.',
    'record_action_error': 'There was a problem while processing this action.',
    'feeds_feed_exist_error': 'Seems like we already have this feed record. Please use search box below in order to find it.'
}

emails = {
    'passwordRecovery': (
        'You are receving this email because you chose to reset password to your %s account.' % settings.domain,
        'You can use the following link within 24 hours to reset your password:',
        'http://www.%s/resetpassword/{token}' % settings.domain,
        '',
        'If you don\'t wish to reset your password and you believe this message was sent to you by mistake,',
        'please ignore this message and no action will be taken.',
        '',
        'Sincerely', 
        settings.domain
        )
}