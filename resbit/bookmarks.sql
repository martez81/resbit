-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: bookmarks
-- ------------------------------------------------------
-- Server version	5.5.38-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Bookmarks`
--

DROP TABLE IF EXISTS `Bookmarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bookmarks` (
  `bookmarkID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `bookmarkName` varchar(256) NOT NULL,
  `bookmarkUrl` varchar(1024) NOT NULL,
  `bookmarkReadLater` tinyint(1) NOT NULL DEFAULT '0',
  `bookmarkPrivate` tinyint(1) NOT NULL DEFAULT '0',
  `bookmarkNotes` text NOT NULL,
  `bookmarkImage` varchar(256) NOT NULL,
  `bookmarkImageCDN` varchar(50) NOT NULL,
  `bookmarkCreatedOn` datetime NOT NULL,
  `bookmarkDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bookmarkID`)
) ENGINE=InnoDB AUTO_INCREMENT=318 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BookmarksNotes`
--

DROP TABLE IF EXISTS `BookmarksNotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BookmarksNotes` (
  `noteID` int(11) NOT NULL AUTO_INCREMENT,
  `bookmarkID` int(11) NOT NULL,
  `noteContent` text NOT NULL,
  `noteCreatedOn` datetime NOT NULL,
  `noteDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Feeds`
--

DROP TABLE IF EXISTS `Feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Feeds` (
  `feedID` int(11) NOT NULL AUTO_INCREMENT,
  `categoryID` int(11) NOT NULL,
  `feedName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `feedTitle` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `feedSubtitle` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `feedTags` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `feedLink` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Website, the owner of the feed',
  `feedUrl` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `feedBanned` tinyint(1) NOT NULL,
  `feedImage` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `feedUpdatedOn` datetime NOT NULL,
  `feedCreatedOn` datetime NOT NULL,
  `feedSubscribedTotal` int(11) NOT NULL,
  PRIMARY KEY (`feedID`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FeedsCategories`
--

DROP TABLE IF EXISTS `FeedsCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FeedsCategories` (
  `categoryID` int(11) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(50) NOT NULL,
  PRIMARY KEY (`categoryID`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FeedsGroups`
--

DROP TABLE IF EXISTS `FeedsGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FeedsGroups` (
  `groupID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `groupName` varchar(128) NOT NULL,
  PRIMARY KEY (`groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FeedsItems`
--

DROP TABLE IF EXISTS `FeedsItems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FeedsItems` (
  `itemID` int(11) NOT NULL AUTO_INCREMENT,
  `feedID` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  `itemHash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `itemTitle` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `itemSummary` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `itemContent` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `itemUrl` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `itemUrlID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `itemTags` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `itemImage` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `itemVideo` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `itemImages` mediumtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'JSON string',
  `itemAuthor` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `itemPublished` datetime NOT NULL,
  `itemCreated` datetime NOT NULL,
  PRIMARY KEY (`itemID`),
  UNIQUE KEY `feedID_3` (`feedID`,`itemHash`),
  KEY `itemUrl` (`itemUrl`(255)),
  KEY `itemPublished` (`itemPublished`),
  KEY `feedID_2` (`feedID`,`itemPublished`),
  KEY `itemHash` (`itemHash`),
  KEY `categoryID` (`categoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=245928 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FeedsScheduled`
--

DROP TABLE IF EXISTS `FeedsScheduled`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FeedsScheduled` (
  `feedID` bigint(20) unsigned NOT NULL,
  `scheduledOn` datetime NOT NULL,
  `scheduledBy` varchar(50) NOT NULL,
  UNIQUE KEY `feedID` (`feedID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Signups`
--

DROP TABLE IF EXISTS `Signups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Signups` (
  `signupID` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL,
  PRIMARY KEY (`signupID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Tags`
--

DROP TABLE IF EXISTS `Tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tags` (
  `tagID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `tagName` varchar(128) NOT NULL,
  `tagColor` char(7) NOT NULL DEFAULT '',
  `tagDeleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tagID`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TagsRelationships`
--

DROP TABLE IF EXISTS `TagsRelationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TagsRelationships` (
  `tagID` int(11) NOT NULL,
  `bookmarkID` int(11) NOT NULL,
  PRIMARY KEY (`tagID`,`bookmarkID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Tasks`
--

DROP TABLE IF EXISTS `Tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tasks` (
  `taskName` varchar(30) NOT NULL,
  `taskUpdatedOn` datetime NOT NULL,
  `taskStaleInterval` int(11) NOT NULL COMMENT 'seconds',
  PRIMARY KEY (`taskName`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `userEmail` varchar(100) NOT NULL,
  `userScreenName` varchar(50) NOT NULL,
  `userPassword` char(60) NOT NULL,
  `userSalt` varchar(40) NOT NULL,
  `userCreatedOn` datetime NOT NULL,
  `userDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userID`),
  UNIQUE KEY `userEmail` (`userEmail`),
  UNIQUE KEY `userEmail_2` (`userEmail`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UsersFeeds`
--

DROP TABLE IF EXISTS `UsersFeeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UsersFeeds` (
  `userID` int(11) NOT NULL,
  `feedID` int(11) NOT NULL,
  `userFeedShowMax` int(11) NOT NULL DEFAULT '5',
  `userFeedColor` char(7) NOT NULL,
  PRIMARY KEY (`userID`,`feedID`),
  CONSTRAINT `UsersFeeds_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UsersFeedsItems`
--

DROP TABLE IF EXISTS `UsersFeedsItems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UsersFeedsItems` (
  `userID` int(11) NOT NULL,
  `itemID` int(11) NOT NULL,
  `userItemPinned` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Read Later',
  `userItemRead` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemID`,`userID`),
  KEY `userID` (`userID`),
  CONSTRAINT `UsersFeedsItems_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UsersRecovery`
--

DROP TABLE IF EXISTS `UsersRecovery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UsersRecovery` (
  `recoveryID` int(11) NOT NULL AUTO_INCREMENT,
  `userEmail` varchar(100) NOT NULL,
  `recoveryToken` varchar(40) NOT NULL,
  `recoveryCreatedOn` datetime NOT NULL,
  PRIMARY KEY (`recoveryID`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UsersSettings`
--

DROP TABLE IF EXISTS `UsersSettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UsersSettings` (
  `userID` int(11) NOT NULL,
  `settingNotify` varchar(40) NOT NULL,
  `settingTimeZone` varchar(50) NOT NULL,
  `settingProfilePrivate` tinyint(4) NOT NULL,
  PRIMARY KEY (`userID`),
  CONSTRAINT `UsersSettings_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-01  3:20:47
