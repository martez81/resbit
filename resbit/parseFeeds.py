#!/usr/bin/env python
import os
import sys
import re
import pdb
import time
import json
import urllib2
import cStringIO
import feedparser
import optparse
import BeautifulSoup as bs4
import urlparse
import hashlib
import logging

from operator import itemgetter
from goose import Goose

from resbit.models import feed, feeds, feedItem
from resbit import settings, url_helper, getImages

"""
['updated', 'published_parsed', 'updated_parsed', 'links', 'title', 'author', 
'summary', 'content', 'guidislink', 'title_detail', 'link', 'authors', u'thr_total', 
'author_detail', 'id', 'tags', 'published']
"""

logger = logging.getLogger('resbit.tasks')

def itemsToProcess(feedID):
    """ Create a list of documents that will be processed.
    """
    f = None
    if feedID:
        f = feed.Feed(feedID)

    articles = []
    if f:
        fp = feedparser.parse(f.feedUrl, agent=url_helper.USER_AGENT)

        entries = fp.entries[:] if fp else []

        feedsObj = feeds.Feeds()
        for key, item in enumerate(entries):
            article = {}

            # Limit to pull only latest len(key) because some feeds publish all entries ever!
            if key == 30:
                logger.warning('Limit exhausted')
                break
            
            itemUrl = item.get('link', '').encode('utf8').strip()
            if itemUrl == '' or item.get('title', '').strip() == '':
                logger.warning('Item has no link or title, skipping; feed ID %s' % feedID)
                continue

            # Check if this entry already exists
            feedHash = hashlib.md5(itemUrl).hexdigest()
            if feedsObj.itemExists(feedID=feedID, itemHash=feedHash):
                logger.warning('Already exists; skipping feed ID %s, %s' % (feedID, feedHash))
                continue

            article['id'] = item.get('id', '').encode('utf8')
            article['link'] = item.get('link', '').encode('utf8')
            article['title'] = item.get('title', '').encode('utf8')
            article['author'] = item.get('author', '').encode('utf8')
            article['tags'] = ', '.join([t['term'].encode('utf8') for t in item.get('tags') if type(t['term']) in (str, unicode)]) if item.get('tags') else ''
            article['published'] = time.strftime('%Y-%m-%d %H:%M:%S' , item['published_parsed']) if item.get('published_parsed') else time.strftime('%Y-%m-%d %H:%M:%S')

            articles.append(article)

    return articles

def processItem(feedID, item):
    """ Process individual documents.
    """

    if int(feedID) <= 0:
        logger.error('Invalid feedID supplied')
        return

    # when site is down try only first 2 items
    unreachable_address_count = 0 
    
    i = feedItem.FeedItem()
    i.db.reuseConnection = False
    i.feedID        = feedID
    i.itemUrl       = item.get('link', '').strip()
    i.itemUrlID     = item.get('id', '').strip()
    i.itemTitle     = item.get('title', '').strip()
    i.itemAuthor    = item.get('author', '')
    i.itemHash      = hashlib.md5(i.itemUrl).hexdigest()
    i.itemTags      = ''
    i.itemSummary   = ''
    i.itemContent   = ''
    i.itemPublished = '0000-00-00 00:00:00'
    i.itemCreated   = time.strftime('%Y-%m-%d %H:%M:%S')
    i.itemImage     = ''
    i.itemImages    = ''

    # create instance of goose and read the article
    g = Goose({'brower_user_agent': url_helper.USER_AGENT, 'parser_class': 'soup'})

    article = None
    try:
        article = g.extract(url=i.itemUrl)
        # Urls that 404 will still create an instance of article.
        # If we find find_url is blank that means resource is not accessible.
        if article.final_url == '' or article.domain == '':
            article = None
    except Exception, e:
        logger.error(sys.stderr, e)

    if not article:
        logger.warning('Cannot parse url for feed %d: %s' % (feedID, i.itemUrl))
        return

    # See if there are tags in goose?
    # If not check if there are tags in feedparser
    if len(article.tags) > 0:
        i.itemTags = ', '.join(article.tags)
    elif item.get('tags'):
        i.itemTags = item.get('tags')

    # Process summary
    # Some feeds store full article as summary
    # We just need couple paragraphs
    i.itemSummary = ''
    if article.cleaned_text:
        i.itemSummary = article.cleaned_text

    # Look for videos
    if len(article.movies) > 0:
        i.itemVideo = article.movies[0].embed_code

    # Select first image from the list
    if article.top_image:
        i.itemImage = article.top_image.src

    # Sometimes published date doesn't exist so we have to create it using current time.
    # We will start with Goose and fallback to feedparser and finally use current date time as published date
    if article.publish_date:
        i.itemPublished = article.publish_date
    elif item.get('published'):
        i.itemPublished = item.get('published')
    else:
        i.itemPublished = i.itemCreated

    i.create()
    logger.info('Created entry for feedID %s: %s' % (feedID, i.itemUrl))

    return i.itemID

def parse(url, agent):
    """ Provide url to RSS feeds and return feedparser object
    """
    try:
        fp = feedparser.parse(url, agent=agent)
        return fp
    except Exception, e:
        logger.error(stderr, e)
        return False

def feedFinder(url):
    """ Identify feed url in submitted arbitrary url which could be rss, atom feed url or 
    url containing links to feeds (any website).
    """
    found_feed = feedInfo(url)

    if found_feed:
        return found_feed
    else:
        resource = url_helper.load_resource(url)
        if resource:
            soup = bs4.BeautifulSoup(resource.read())
            soup_rss = soup.findAll('link', type=('application/rss+xml', 'application/atom+xml'))

            if soup_rss:
                rss_href = soup_rss[0].get('href')
                if rss_href:
                    rss_abs_url = url_helper.absolute_url(rss_href, url)
                    found_feed = feedInfo(rss_abs_url)
                    if found_feed:
                        return found_feed

    return None

def feedInfo(feedUrl):
    """
    Purpose of this function is to get information about the feed.
    Used by:
    - feeds._saveNewFeed
    - parseFeeds.feedFinder
    """
    # Fix Url
    if (feedUrl.find('http://') < 0 and 
        feedUrl.find('https://') < 0):
        feedUrl = 'http://'+str(feedUrl)

    feedInfo = dict()
    fp = parse(feedUrl, agent=url_helper.USER_AGENT)
    if not fp:
        return False

    if len(fp.entries) == 0:
        return None

    feedInfo['feedTitle']       = fp.feed.get('title', '')
    feedInfo['feedSubtitle']    = fp.feed.get('subtitle', '')
    feedInfo['feedLink']        = fp.feed.get('link', '')
    feedInfo['feedUrl']         = feedUrl
    feedInfo['feedTags']        = ', '.join([t['term'] for t in fp.feed.get('tags', list())])
    feedInfo['feedImage']       = ''

    if fp.feed.get('image'):
        feedInfo['feedImage'] = fp.feed['image'].get('href', '')

    return feedInfo

def findImages(url, html, html_chunk):
    """ Find most related images on a page. Currently used by bookmarks only.
    For feeds goose is used to extract main image of the article.
    """
    images = []
    base_url = url_helper.get_host_for_url(url)

    soup_full = bs4.BeautifulSoup(html)
    soup_chunk = bs4.BeautifulSoup(html_chunk)

    # soup_chunk is most prefered content to look for images in
    candidates_chunk = getImages.find_image_candidates(soup_chunk)
    candidates_ogimage = getImages.find_ogimage_candidates(soup_full)

    images = getImages.process_candidates(candidates_chunk + candidates_ogimage, base_url=base_url)

    if len(images) < 3:
        candidates_chunk_up = getImages.find_image_candidates(soup_full, soup_chunk=soup_chunk, direction='up')
        candidates = candidates_chunk + candidates_chunk_up
        images = getImages.process_candidates(candidates, base_url=base_url)

    return images


if __name__ == '__main__':
    activate_this = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../venv/bin/activate_this.py')
    execfile(activate_this, dict(__file__=activate_this))
    del activate_this

    p = optparse.OptionParser("""
        Available functions are: 
        - 
        - queueFeed(feedid), 
        - consumeQueue, 
        - parseFeed(feedid) if feedid is given all feeds will be processed
        """)

    p.add_option('--function', '-f', default='parseFeed')
    p.add_option('--feedid', default=None, help='Optional for "parseFeed"')
    options, args = p.parse_args()

    if options.function:
        func = options.function

        if func == 'parseFeed':
            parseFeed(options.feedid)


