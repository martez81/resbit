#!/usr/bin/env python

import urllib2
import optparse
import pdb
import cStringIO
import mimetypes
import imghdr
import sys
from urlparse import urlparse, urljoin

USER_AGENT = 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:20.0) Gecko/20100101 Firefox/20.0'

def get_mimetype(url_or_file):
	mimetype_map = {
		'png': 'image/png',
		'jpg': 'image/jpeg',
		'jpeg': 'image/jpg'

	}

	"""
	>>> get_mimetype('http://www.codinghorror.com/.a/6a0120a85dcdae970b019101a6e7b7970c-800wi')
	'png'

	>>> get_mimetype('http://cdn.petapixel.com/assets/uploads/2013/06/babymugging4.jpg')
	'jepg'
	"""

	mimetype = mimetypes.guess_type(url_or_file, strict=False)[0]

	if not mimetype:
		# print 'Couldn\'t guess, defaulting to read file contents'
		resource = load_resource(url_or_file)
		if resource:
			file = cStringIO.StringIO(resource.read())
			mimetype = mimetype_map.get(imghdr.what(file))
		else:
			mimetype = None

	return mimetype


def get_extension(url_or_file):
	return mimetypes.guess_extension(url_or_file, strict=False)

def fix_url_schema(url):
	url = url.strip()
	if url.find('https://') == 0 or url.find('http://') == 0:
		return url
	else:
		return 'http://%s' % url


def get_host_for_url(url, no_scheme=False):
	"""
	>>> get_host_for_url('invalid')
	'http://invalid'

	>>> get_host_for_url('http://base.com/whatever/fdsh')
	'http://base.com'

	>>> get_host_for_url('http://base.com/whatever/fdsh', no_scheme=True)
	'base.com'

	>>> get_host_for_url('base.com/whatever/fdsh.jpg')
	'http://base.com'

	>>> get_host_for_url('https://base.com/whatever/fdsh.jpg')
	'https://base.com'

	"""

	# Fix url before doing anything
	if not url.startswith('http'):
		url = 'http://%s' % url

	url_parsed = urlparse(url)

	host = None
	if no_scheme:
		host = url_parsed[1]
	elif url_parsed[0] and url_parsed[1]:
		host = '://'.join(url_parsed[:2])

	if not host:
		# print("could not extract host from URL: %r" % (url,))
		return None
	return host


def absolute_url(url, base_url, no_query=False):
	"""
	>>> absolute_url('/images/foo.jpg', 'base.com/whatever.html')
	'http://base.com/images/foo.jpg'
	
	>>> absolute_url('foo', 'http://base/whatever/ooo/fdsh')
	'http://base/whatever/ooo/foo'

	>>> absolute_url('foo/bar/', 'http://base')
	'http://base/foo/bar/'

	>>> absolute_url('/foo/bar', 'http://base/whatever/fdskf')
	'http://base/foo/bar'

	>>> absolute_url('\\n/foo/bar', 'http://base/whatever/fdskf')
	'http://base/foo/bar'

	>>> absolute_url('http://localhost/foo', 'http://base/whatever/fdskf')
	'http://localhost/foo'

	>>> absolute_url('http://tctechcrunch2011.files.wordpress.com/2013/06/velodroom-stand-closeup.jpg?w=1024&h=682', 'pixel.quantserve.com', no_query=True)
	'http://tctechcrunch2011.files.wordpress.com/2013/06/velodroom-stand-closeup.jpg'
	"""

	url = url.strip()
	url_parsed = urlparse(url)
	
	if no_query and url_parsed.query:
		url = url[:len(url)-(len(url_parsed.query)+1)]
	
	if url_parsed[0]:
		return url

	# fix base_url before doing anything
	if not base_url.startswith('http'):
		base_url = 'http://%s' % base_url

	base_url_parts = urlparse(base_url)
	base_server = '://'.join(base_url_parts[:2])

	if url.startswith('/'):
		return base_server + url
	else:
		path = base_url_parts[2]
		if '/' in path:
			path = path.rsplit('/', 1)[0] + '/'
		else:
			path = '/'
		return base_server + path + url


def load_resource(url, timeout=3, retry=True):
	resource = None
	try:
		request = urllib2.Request(url)
		request.add_header('User-Agent', USER_AGENT)
		opener = urllib2.build_opener()
		resource = opener.open(request, timeout=timeout)
	except urllib2.URLError:
		# pdb.set_trace()
		if retry:
			print >> sys.stderr, 'Trying again'
			resource = load_resource(url, retry=False, timeout=3)
		return None
	return resource


if __name__ == '__main__':
	p = optparse.OptionParser()
	p.add_option('--function', '-f')
	p.add_option('--url', help='')
	options, args = p.parse_args()

	if options.function:
		func = options.function

		if func == 'test':
			import doctest
			doctest.testmod()
		elif func == 'get_mimetype':
			get_mimetype(options.url)
