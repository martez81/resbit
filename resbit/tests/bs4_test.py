import sys
import urllib2
from bs4 import BeautifulSoup, Comment
import pdb

USER_AGENT = 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:20.0) Gecko/20100101 Firefox/20.0'

HTML = """
<html>
	<head>
	</head>
	<body>
		<div id="content">
			<h1>Header 1</h1>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
			<img src="http://pythonconquerstheuniverse.wordpress.com/wp-content/themes/pub/twentyeleven/images/headers/shore.jpg?m=1354160568g" />
			<p>Above is some cool image</p>
		</div>
		<div id="left">
			<img src="http://auslieferung.commindo-media-ressourcen.de/www/delivery/ai.php?filename=300x250-tryitfree_2.png&contenttype=png"/>
		</div>
	</body>
</html>
"""

def loadResource(src):
    request = urllib2.Request(src)
    request.add_header('User-Agent', USER_AGENT)
    opener = urllib2.build_opener()
    resource = opener.open(request)

    return resource

def main(url=None, html=HTML):

	if url:
		resource = loadResource(url)
		html = resource.read()

	soup = BeautifulSoup(html)
	soupH1 = soup.find('h1')

	pdb.set_trace()

if __name__ == '__main__':
	main(sys.argv[1])