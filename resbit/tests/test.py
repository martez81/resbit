from flask import Flask, request, session, redirect, flash, render_template, get_flashed_messages, url_for
from tornado import httpserver, wsgi, ioloop, gen
import os
import re
import sys
import time
import datetime

# Add custom modules to system path list
# sys.path.append(os.path.dirname(__file__))
# sys.path.append(os.path.dirname(__file__) + "/modules")


# Create flask application object
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret key'
app.config.from_object(__name__)

data = ''

@app.route('/')
def index():
    global data
    time.sleep(10)
    data = request.args.get('data', '')
    # return 'set data: ' + data, 202
    yield gen.Task('set data: ' + data)

@app.route('/get_data')
def findData():
    global data
    # return 'get global data: ' + data, 202
    yield gen.Task('set data: ' + data)

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0')
    autoreload.start()
    # server = httpserver.HTTPServer(wsgi.WSGIContainer(app))
    # server.listen(5000)
    # ioloop.IOLoop.instance().add_callback(findData)
    # ioloop.IOLoop.instance().start()