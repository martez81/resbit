#!/usr/bin/env python
import os
import parseFeeds
import pydbwrapper
import pdb
import sys
import hashlib
import socket
import time
import traceback
from resbit import settings, tasks, models, celeryconfig
from celery import group, Celery

logger = tasks.logger
celery = Celery('tasks')
celery.config_from_object(celeryconfig)

def scheduleTasks(limit=30, feedID = None, wait=False):
    """ This is triggered by looksForTasks() Celery Beat and
    can also be triggered form the front-end when user 
    enters new feed into the system. New feed will be processed
    on high priority queue "feeds_hipri". Everything else goes to regular queue "feeds".
    """
    try:
        models.feeds.db.reuseConnection = False

        nodeName = socket.gethostname()
        if not nodeName or nodeName == '':
            print 'Cannot get node name.'
            return False
        
        feeds = models.feeds.Feeds()
        
        # default queue
        queue = 'feeds'

        # process only selected feed
        if feedID:
            queue = 'feeds_hipri'
            feedsToProcess = [{'feedID': int(feedID)}]
        else:
            # get all marked feeds to process
            feedsToProcess = feeds.getScheduledFeeds(nodeName, limit=limit)
            if not feedsToProcess:
                # secure a pool of records
                feeds.assignFeeds(nodeName, limit=limit)
                feedsToProcess = feeds.getScheduledFeeds(nodeName, limit=limit)

        timeStart = time.time()

        for feed in feedsToProcess:
            job = tasks.scheduleFeeds.apply_async((feed,queue), link=tasks.updateFeed.s())

            if wait:
                result = job.get()

                
    except:
        logger.error(traceback.format_exc())

@celery.task(ignore_result=True)
def queueFeeds(olderThan=3600, limit=None):
    """ Used by Celery Beat. 
    Finds all feeds that need to be updated and puts
    them on the queue (in mysql table) signing them with server
    signature (hostname). This will be only executed by master node - check celeryconfig.
    """
    try:
        models.feeds.db.reuseConnection = False
        olderThan = int(olderThan)
        feeds = models.feeds.Feeds()
        feedsData = feeds.getFeeds(olderThan=olderThan, limit=limit, excludeScheduled=True)

        if feedsData:
            feeds.queueFeeds(feedsData)
            print('%s feeds queued at %s' % (len(feedsData), time.strftime('%Y-%m-%d %H:%M:%S')))

        return True
    except:
        logger.error(traceback.format_exc())

@celery.task(ignore_result=True)
def lookForTasks():
    """ Used by Celery Beat. 
    This will check if there are any parent/feeds tasks that can be processed.
    If there are they will be put on the queue. This should run quite often
    to make sure nodes always have something to do.
    """
    scheduleTasks()


if __name__ == '__main__':
    import os
    activate_this = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../venv/bin/activate_this.py')
    execfile(activate_this, dict(__file__=activate_this))
    
    if len(sys.argv) == 2 and sys.argv[1].isdigit():
        scheduleTasks(0, sys.argv[1])
    elif len(sys.argv) == 2 and sys.argv[1] == 'lookForTasks':
        lookForTasks()
    elif len(sys.argv) == 2 and sys.argv[1] == 'queueFeeds':
        queueFeeds()
        
