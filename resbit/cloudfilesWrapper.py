#!/home/marcin/pyProjects/bookmarks/venv/bin/python

import cloudfiles
import settings
import sys
import pdb
import os
import cStringIO
import urllib2

class CloudfilesWrapper():

	MIMETYPES = {
		'.jpg': 'image/jpeg',
		'.png': 'image/png',
		'.gif': 'image/gif',
		'.tiff': 'image/tiff'
	}

	def __init__(self, cfConfig, container):
		self.config = cfConfig
		self.conn = None
		self.container = None
		self.file_contents = None

		self.connect(container)

	def connect(self, container):
		self.conn = cloudfiles.get_connection(self.config['username'], self.config['api_key'])
		self.container = self.conn.get_container(container)

	def load_file(self, source):
		if type(source) == cStringIO.OutputType:
			return source
		elif type(source) == str:
			if (source.find('http://') >= 0
				or source.find('https://') >= 0):
				return cStringIO.StringIO(urllib2.urlopen(source).read())
			else:
				return open(source)

		return False

	def put(self, file_name, ext, source):
		file_contents = self.load_file(source)

		cfobj = self.container.create_object(file_name)
		
		if ext in self.MIMETYPES:
			cfobj.content_type = self.MIMETYPES[ext]

		cfobj.write(file_contents.read())

		return True

	def get(self, name):
		pass

	def delete(self, name):
		pass

if __name__ == '__main__':

	if len(sys.argv) == 2:
		
		source = sys.argv[1]

		cf = CloudfilesWrapper(settings.cfConfig, 'resbit_img_bookmarks')
		cf.put('text', source)
		print 'done'
