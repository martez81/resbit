#!/home/marcin/pyProjects/bookmarks/venv/bin/python

import os
import time

"""
"""
def cleanTemp():
	path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'bookmarks', 'static', 'pics', 'temp')

	for file in os.listdir(path):
		filePath = os.path.join(path, file)
		fileTime = os.path.getmtime(filePath)
		if time.time() - fileTime > 3600:
			os.remove(filePath)


if __name__ == '__main__':
	print cleanTemp()