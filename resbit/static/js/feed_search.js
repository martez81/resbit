$(document).ready(function() {
	$('form.form-add').on('submit', function(event) {
		search();
		return false;
	});

	$('.feed-refresh').on('click', function(e) {
		e.preventDefault();
		var feedID = $(this).data('id');
		var that = $(this);
		var time = null;
		var counter = 0;
		var quitAfter = 6;

		// Add load icon
		$('<span class="loading-icon">&nbsp;<img src="/static/images/loading.gif" /></span>').appendTo(that.remove('span'));
		
		$.get('/api/feed_refresh/'+feedID, function(data) {
			var datajson = $.parseJSON(data);
			var time = getTime(datajson);
			var newTime = null;

			// $.get('/api/feed_refresh/'+feedID, function() {});
		
			if (time >= 0 && counter <= quitAfter) {
				// Polling funtion
				(function poll(){
					setTimeout(function() {
						counter++;
					    $.ajax(
					    { 
					    	url: '/api/feed/'+feedID,
					    	dataType: "json", 
						    method: 'GET', 
						    timeout: 5000,
						    error: function(XMLHttpRequest, textStatus, errorThrown) 
						    {
				                console.log("An error has occurred when making the request: " + errorThrown);
				        	}
						})
						.done(function(data) {
							datajson = $.parseJSON(data);
							newTime = getTime(data);
	
					    	if (time == newTime)
					    	{
								poll();
					    	} else {
								$(that).find('.loading-icon').remove();
								$(that).parent().find('.feed-feedUpdatedOn').html(data['content']['feedUpdatedOnFormatted']);
					    	}
						});
					}, 3000);
				})();

			} else {
				// print message that everything is up to date
				$(that).find('.loading-icon').remove();
				$(that).parent().find('.feed-feedUpdatedOn').html(data['content']['feedUpdatedOnFormatted']);
			}
		})
	})

	function search()
	{
		$('<span class="loading-search-icon">Searching... &nbsp;<img src="/static/images/loading.gif" /></span>').appendTo('#feed-search-result-wrapper');
		$.get('/api/find_feeds', $('form.form-add').serialize(), function(data) {
			data = $.parseJSON(data);
			$('#feed-search-result').html('');
			$('#tmpl-search-result').tmpl(data).appendTo("#feed-search-result");
			$('.loading-search-icon').remove();
		});
	}

	function getTime(data) 
	{
		if (data.length == 0) {
			return false;
		}

		if (data['content'] && data['responseMessage'] == 'success') {
			time = data['content']['feedUpdatedOnUnixtime'];
			return time;
		} else {
			return false;
		}
	}
});