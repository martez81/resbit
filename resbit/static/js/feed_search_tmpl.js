{{each(i,feed) content}}
<li class="search-result-item" data-feedid="${feed.feedID}" data-feedurl="${feed.feedUrl}">
	<div class="search-result-name">
		<img src="https://plus.google.com/_/favicon?domain=${feed.feedLink}&alt=feed"/>
		<span>&nbsp;${feed.feedTitle}</span>
	</div>
	<div class="search-result-desc">
		${feed.feedSubtitle}
	</div>
	<div class="search-result-links">
		<ul>
			{{if feed.feedID }}
			<li><a href="/feed/${feed.feedID}" target="_blank">View Entries</a>&nbsp;&nbsp;</li>
			<li><a href="/feed-subscribe?id=${feed.feedID}" >Subscribe</a></li>
			{{else}}
			<li><a href="/feed-subscribe?url=${feed.feedUrl}" >Subscribe</a></li>
			{{/if}}
		</ul>
	</div>
</li>
{{/each}}