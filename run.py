#!/usr/bin/env python
import os
activate_this = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'venv/bin/activate_this.py')
execfile(activate_this, dict(__file__=activate_this))
del activate_this

from resbit import app

if __name__ == '__main__':
	app.debug=True
	from tornado.wsgi import WSGIContainer
	from tornado.httpserver import HTTPServer
	from tornado.ioloop import IOLoop

	http_server = HTTPServer(WSGIContainer(app))
	http_server.listen(80)
	IOLoop.instance().start()
	


